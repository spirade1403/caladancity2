using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using static NetworkManager;

public class TimeEvent : MonoBehaviour
{
    public GameObject healthBar;
    private NetworkManager n;
    List<FarmPlantJSON> listPlants;

    private bool inProgress;
    private DateTime TimeStart;
    private DateTime TimeEnd;
    private int Days;
    private int Hours;
    private int Minutes;
    private int Seconds;
    private int itemNumber;
    private string plot;
    private DateTime startTime;
    private float growingTime;

    #region Unity methods
    IEnumerator Start()
    {
        yield return new WaitForSeconds(3f);
        GameObject networkManager = GameObject.Find("Network Manager");
        n = networkManager.GetComponent<NetworkManager>();
        listPlants = n.farmPlants.data;

        if (this.transform.parent.childCount == 3)
        {
            plot = this.transform.parent.parent.name;
            String plotId = Regex.Replace(plot, @"\D", "");

            for (int i = 0; i < listPlants.Count; i++)
            {
                if (listPlants[i].plot_id == Int32.Parse(plotId))
                {
                    itemNumber = i;
                    break;
                }
            }
        }
        else
        {
            itemNumber = 0;
        }


        startTime = DateTime.ParseExact(listPlants[itemNumber].start_time, "yyyy-MM-dd HH:mm:ss", null);
        growingTime = listPlants[itemNumber].growing_time;
        TimeSpan t = TimeSpan.FromSeconds(growingTime);
        Days = t.Days;
        Hours = t.Hours;
        Minutes = t.Minutes;
        Seconds = t.Seconds;
    }

    void Update()
    {
        GameObject networkManager = GameObject.Find("Network Manager");
        n = networkManager.GetComponent<NetworkManager>();
        listPlants = n.farmPlants.data;

        if (this.transform.parent.childCount == 3)
        {
            plot = this.transform.parent.parent.name;
            String plotId = Regex.Replace(plot, @"\D", "");

            for (int i = 0; i < listPlants.Count; i++)
            {
                if (listPlants[i].plot_id == Int32.Parse(plotId))
                {
                    itemNumber = i;
                    break;
                }
            }
        }
        else
        {
            itemNumber = 0;
        }


        startTime = DateTime.ParseExact(listPlants[itemNumber].start_time, "yyyy-MM-dd HH:mm:ss", null);
        growingTime = listPlants[itemNumber].growing_time;
        TimeSpan t = TimeSpan.FromSeconds(growingTime);
        Days = t.Days;
        Hours = t.Hours;
        Minutes = t.Minutes;

        DateTime start = DateTime.Now;
        TimeSpan time = new TimeSpan(Days, Hours, Minutes, Seconds);
        TimeStart = startTime;
        TimeEnd = TimeStart.Add(time);
        TimeSpan timeLeft = TimeEnd - start;
        double totalSecondsLeft = timeLeft.TotalSeconds;
        double totalSeconds = growingTime - (DateTime.Now - startTime).TotalSeconds;
        string text;

        text = "";
        if (totalSeconds > 1)
        {
            if (timeLeft.Days > 0)
            {
                text += timeLeft.Days + "d ";
                text += timeLeft.Hours + "h ";
            }
            else if (timeLeft.Hours > 0)
            {
                text += timeLeft.Hours + "h ";
                text += timeLeft.Minutes + "m ";
            }
            else if (timeLeft.Minutes > 0)
            {
                TimeSpan ts = TimeSpan.FromSeconds(totalSecondsLeft);
                text += ts.Minutes + "m ";
                text += ts.Seconds + "s ";
            }
            else if (timeLeft.Seconds > 0)
            {
                text += Mathf.FloorToInt((float)totalSecondsLeft) + "s";
            }
            gameObject.GetComponent<TextMesh>().text = text;
            totalSecondsLeft -= Time.deltaTime;
        }
        else if (totalSeconds == 0)
        {
            gameObject.GetComponent<TextMesh>().text = "Finished";
        }
        else
        {
            gameObject.GetComponent<TextMesh>().text = "";
        }
        
    }
    #endregion

}
