﻿
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="so_ItemList", menuName="Scriptable Objects/Item/Item List")]
public class SO_ItemList : ScriptableObject
{
    [SerializeField]
    public List< ItemDetails> itemDetails;

    public int ItemCount
    {
        get
        {
            return itemDetails.Count;
        }
    }

    public ItemDetails GetItem(int index)
    {
        return itemDetails[index];
    }

    public ItemDetails GetItemByItemNumber(int itemCode)
    {
        for(int i = 0; i< itemDetails.Count; i++)
        {
            if(itemDetails[i].itemCode == itemCode)
            {
                return itemDetails[i];
            }
        }
        return null;
    }

    public int countItemQuantity()
    {
        int sum = 0;
        for (int i = 0; i < itemDetails.Count; i++)
        {
            sum += itemDetails[i].quantity;
        }
        return sum;
    }
}
