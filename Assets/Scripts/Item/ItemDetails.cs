﻿
using UnityEngine;

[System.Serializable]
public class ItemDetails 
{
    public int itemCode;
    public ItemType itemType;
    public string itemDescription;
    public string itemName;
    public Sprite itemSprite;
    public string itemLongDescription;
    public short itemUseGridRadius;
    public float itemUseRadius;
    public bool isStartingItem;
    public bool canBePickedUp;
    public bool canBeDropped;
    public bool canBeEaten;
    public bool canBeCarried;
    public int itemPrice;
    public string spriteLink;
    public string frontArmSprite;
    public string backArmSprite;
    public int quantity;
    public string animator;

    public ItemDetails()
    {

    }

    public ItemDetails(int itemCode, ItemType itemType, string itemDescription, Sprite itemSprite, string itemLongDescription, short itemUseGridRadius, 
        float itemUseRadius, bool isStartingItem, bool canBePickedUp, bool canBeDropped, bool canBeEaten, bool canBeCarried, int itemPrice, string spriteLink)
    {
        this.itemCode = itemCode;
        this.itemType = itemType;
        this.itemDescription = itemDescription;
        this.itemSprite = itemSprite;
        this.itemLongDescription = itemLongDescription;
        this.itemUseGridRadius = itemUseGridRadius;
        this.itemUseRadius = itemUseRadius;
        this.isStartingItem = isStartingItem;
        this.canBePickedUp = canBePickedUp;
        this.canBeDropped = canBeDropped;
        this.canBeEaten = canBeEaten;
        this.canBeCarried = canBeCarried;
        this.itemPrice = itemPrice;
        this.spriteLink = spriteLink;
    }
}
