using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishMove : MonoBehaviour
{
    private bool moveRight = true;
    private float moveTime = 0;
    private float vRange = 0.005f;


    [Header("Detection Fields")]
    //Detection Point
    public Transform detectionPoint;
    //Detection Radius
    private const float detectionRadius = 0.5f;
    //Detection Layer
    public LayerMask detectionLayer;
    //Cached Trigger Object
    public GameObject detectedObject;


    // Start is called before the first frame update
    void Start()
    {
        vRange = Random.Range(-0.005f, 0.005f);
    }

    // Update is called once per frame
    void Update()
    {
        moveTime -= Time.deltaTime;
        if(moveTime <= 0)
        {
            moveTime = Random.Range(1f, 2f);
            moveRight = !moveRight;
            if(moveRight)
            {
                GetComponent<Transform>().localScale = new Vector3(-1, 1, 1);
            }
            else
            {
                GetComponent<Transform>().localScale = new Vector3(1, 1, 1);
            }
        }

        //Check collide
        Vector3 currentPos = GetComponent<Transform>().position;
        if(currentPos.x < -9.5)
        {
            moveRight = false;
            GetComponent<Transform>().localScale = new Vector3(1, 1, 1);

        }
        if(currentPos.x > 1.5)
        {
            moveRight = true;
            GetComponent<Transform>().localScale = new Vector3(-1, 1, 1);
        }
        if(currentPos.y < -4.8 && vRange < 0)
        {
            vRange = -vRange;
        }
        if (currentPos.y > 0 && vRange > 0)
        {
            vRange = -vRange;
        }

        //Update position
        if (!moveRight)
        {
            GetComponent<Transform>().position += new Vector3(0.02f, vRange, 0);
        }
        else
        {
            GetComponent<Transform>().position += new Vector3(-0.02f, vRange, 0);
        }

        //Dectection
        if (DetectObject())
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                Destroy(gameObject);
            }
        }
    }

    bool DetectObject()
    {

        Collider2D obj = Physics2D.OverlapCircle(detectionPoint.position, detectionRadius, detectionLayer);

        if (obj == null)
        {
            detectedObject = null;
            return false;
        }
        else
        {
            detectedObject = obj.gameObject;
            return true;
        }
    }


}
