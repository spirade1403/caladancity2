using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundedNPC : MonoBehaviour
{
    private Vector3 directionVector;
    public Transform transform;
    public float speed;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        // transform = getComponent<Transform>();
        ChangeDirection();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        rb.MovePosition(transform.position + directionVector * speed * Time.deltaTime );
    }

    void ChangeDirection()
    {
        int direction = Random.Range(0, 4);
        switch (direction)
        {
            case 0: //right
                directionVector = Vector3.right;
                break;
            case 1: //up
                directionVector = Vector3.up;
                break;
            case 2: //left
                directionVector = Vector3.left;
                break;
            case 3: //down
                directionVector = Vector3.down;
                break;
            default:
                break;
        }
    }
}
