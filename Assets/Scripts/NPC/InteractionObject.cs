using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionObject : MonoBehaviour
{
    public bool inventory;
    public bool openable;
    public bool locked;
    public bool talks;

    public GameObject itemNeeded;

    public Animator anim;

    public string message;
    
    public void DoInteraction()
    {
        gameObject.SetActive(false);
    }
   
    public void Open()
    {
        anim.SetBool("open", true);
    }
}
