using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChatBubble : MonoBehaviour
{

    public static void Create(Transform parent, Vector3 localPosition, IconType iconType, string text)
    {
        Transform chatBubbleTransform =  Instantiate(GameAssets.Instances.pfChatBubble, parent);
        chatBubbleTransform.localPosition = localPosition;

        chatBubbleTransform.GetComponent<ChatBubble>().SetUp(iconType, text);

        Destroy(chatBubbleTransform.gameObject, 4f);
    }
    public enum IconType
    {
        Happy,
        Neutral,
        Angry
    }

    private SpriteRenderer backGroundSpriteRenderer;
    private SpriteRenderer iconSpriteRenderer;
    private TextMeshPro textMeshPro;

    [SerializeField] private Sprite angryIconSprite;
    [SerializeField] private Sprite happyIconSprite;
    [SerializeField] private Sprite NeutralIconSprite;

    

    private void Awake()
    {
        backGroundSpriteRenderer = transform.Find("BackGround").GetComponent<SpriteRenderer>();
        iconSpriteRenderer = transform.Find("Icon").GetComponent <SpriteRenderer>();
        textMeshPro = transform.Find("Text").GetComponent<TextMeshPro>();
    }


    private void SetUp(IconType iconType, string text)
    {
        textMeshPro.SetText(text);
        textMeshPro.ForceMeshUpdate();
        Vector2 textSize = textMeshPro.GetRenderedValues(false);

        Vector2 padding = new Vector2(7f, 3f);
        backGroundSpriteRenderer.size = textSize + padding;

        Vector3 offset = new Vector3(-14f, 0f);
        backGroundSpriteRenderer.transform.localPosition =
            new Vector3(backGroundSpriteRenderer.size.x / 2f, 0f) + offset;

        iconSpriteRenderer.sprite = GetIconSprite(iconType);
    }

    private Sprite GetIconSprite(IconType iconType)
    {
        switch (iconType)
        {
            default: 
            case IconType.Happy: return happyIconSprite;
            case IconType.Neutral: return NeutralIconSprite;
            case IconType.Angry: return angryIconSprite;
        }
    }
}
