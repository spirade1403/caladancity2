using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FarmItemList", menuName = "Scriptable Objects/Item/Farm Item List")]
public class FarmItemList : ScriptableObject
{
    [SerializeField]
    public List<FarmItemDetail> itemDetails;

    public int ItemCount
    {
        get
        {
            return itemDetails.Count;
        }
    }

    public FarmItemDetail GetItem(int index)
    {
        return itemDetails[index];
    }

    public FarmItemDetail GetItemByItemNumber(int itemCode)
    {
        for (int i = 0; i < itemDetails.Count; i++)
        {
            if (itemDetails[i].itemCode == itemCode)
            {
                return itemDetails[i];
            }
        }
        return null;
    }

    public FarmItemDetail GetItemByPlotName(string plotName)
    {
        for (int i = 0; i < itemDetails.Count; i++)
        {
            if (itemDetails[i].plot.Equals(plotName))
            {
                return itemDetails[i];
            }
        }
        return null;
    }

    public int countItemQuantity()
    {
        int sum = 0;
        for(int i = 0; i < itemDetails.Count; i++)
        {
            sum += itemDetails[i].itemQuantity;
        }
        return sum;
    }
}
