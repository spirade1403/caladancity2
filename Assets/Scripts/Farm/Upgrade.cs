using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
using System;

public class Upgrade : MonoBehaviour
{
    Vector3Int position;
    Vector3 newPosition;

    public Tile soil;
    public Tilemap tilemap;
    public GameObject popUpBox;
    public GameObject popUpText;
    public GameObject newSign;
    public GameObject plot;
    [SerializeField] public Text numberOfPlot;
    private bool isUpgrade = false;

    public void Paint()
    {
        Int32 x = Convert.ToInt32(gameObject.transform.position.x - 0.5);
        Int32 y = Convert.ToInt32(gameObject.transform.position.y - 0.5);
        Int32 z = Convert.ToInt32(gameObject.transform.position.z);
        position = new Vector3Int(x, y, z);
        
        for(int i = 0; i < 6; i++)
        {
            tilemap.SetTile(new Vector3Int(position.x + i, position.y, position.z), soil);
            tilemap.SetTile(new Vector3Int(position.x + i, position.y - 1, position.z), soil);
        }
        
    }

    private void Update()
    {
        //isUpgrade = false;
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null)
            {
                if(hit.collider.tag == "Sign")
                {
                    PopUp("Upgrade more slots?");                    
                }
            }
        }

        if (isUpgrade)
        {
            Paint();

            // Add active plot
            for (int i = Int32.Parse(numberOfPlot.text)-1; i< Int32.Parse(numberOfPlot.text) +3; i++)
            {

            }
            int num = Int32.Parse(numberOfPlot.text) + 3;
            numberOfPlot.text = num.ToString();
            
            if (gameObject.transform.position.x < 16 && gameObject.transform.position.y < 1)
            {

                newPosition = new Vector3(gameObject.transform.position.x + 7, gameObject.transform.position.y, gameObject.transform.position.z);
                gameObject.transform.position = newPosition;
            }
            else if (gameObject.transform.position.y < 1)
            {
                newPosition = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 3, gameObject.transform.position.z);
                gameObject.transform.position = newPosition;
            }
            else
            {
                newPosition = new Vector3(gameObject.transform.position.x - 7, gameObject.transform.position.y, gameObject.transform.position.z);
                gameObject.transform.position = newPosition;
            }
            AddPlot();
            isUpgrade = false;
        }
    }

    public void PopUp(string text)
    {
        popUpText.GetComponent<UnityEngine.UI.Text>().text = text;
        popUpBox.SetActive(true);
    }

    public void Confirm()
    {
        isUpgrade = true;
        popUpBox.SetActive(false);
    }

    public void Cancel()
    {
        isUpgrade = false;
        popUpBox.SetActive(false);
    }

    public void AddPlot()
    {         
        for(int i = 0; i < 3; i++)
        {
            Int32 x = Convert.ToInt32(gameObject.transform.position.x + 2*i);
            Int32 y = Convert.ToInt32(gameObject.transform.position.y);
            Int32 z = Convert.ToInt32(gameObject.transform.position.z);
            Vector3 plotPosition = new Vector3(x, y, z);
            Instantiate(plot, plotPosition, gameObject.transform.rotation);
        }        
    }

}
