using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class SoilDetailDB 
{
    public int soilID;
    public int userID;
    public string soilStatus;
}
