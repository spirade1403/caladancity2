using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoilList", menuName = "Scriptable Objects/Soil/Soil List")]

public class SoilList : ScriptableObject
{
    [SerializeField]
    public List<SoilDetailDB> itemDetails;

    public int ItemCount
    {
        get
        {
            return itemDetails.Count;
        }
    }

    public SoilDetailDB GetItem(int index)
    {
        return itemDetails[index];
    }

}
