using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Tilemaps;
using static NetworkManager;


public class SoilGroup : MonoBehaviour
{

    public List<SoilDetail> soilDetails;
    private List<FarmPlantJSON> listPlants;

    Vector3Int position;
    Vector3 newPosition;
    [SerializeField] public GameObject oldSign;
    public UserInfoJSON userInfo;
    NetworkManager n;
    public Tile soil;
    public Tilemap tilemap;
    public GameObject popUpBox;
    public GameObject popUpText;
    public GameObject newSign;
    public GameObject plot;
    private bool isUpgrade = false;
    [SerializeField] public GameObject notiCanvas;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(3f);

        GameObject networkManager = GameObject.Find("Network Manager");
        n = networkManager.GetComponent<NetworkManager>();
        userInfo = n.userInfo;

        listPlants = n.farmPlants.data;
        print("listPlants Count:" + listPlants.Count) ;
        int numberOfPlot = userInfo.plot_own;
        if(numberOfPlot == 15)
        {
            oldSign.transform.position = new Vector3((float)16.5 , (float)-1.5, 0);
            Paint(9, -2, 0);
        }
        else if (numberOfPlot == 18)
        {
            oldSign.transform.position = new Vector3((float)9.5, (float)1.5, 0);
            Paint(9, -2, 0);
            Paint(16, -2, 0);
        }
        else if (numberOfPlot == 21)
        {
            oldSign.transform.position = new Vector3((float)16.5, (float)1.5, 0);
            Paint(9, -2, 0);
            Paint(16, -2, 0);
            Paint(9, 1, 0);
        }
        else if (numberOfPlot == 24)
        {
            oldSign.transform.localScale = Vector3.zero;
            Paint(9, -2, 0);
            Paint(16, -2, 0);
            Paint(9, 1, 0);
            Paint(16, 1, 0);
        }

        for (int i = 0; i < userInfo.plot_own; i++)
        {
            soilDetails[i].gameObject.SetActive(true);
            soilDetails[i].gameObject.transform.GetChild(1).GetChild(2).GetComponent<Text>().text = (i + 1)+"";
        }

        for (int i = 0; i < listPlants.Count; i++)
        {
            print("open slot " +listPlants[i].plot_id);
            GameObject plot = GameObject.Find("Plot (" + listPlants[i].plot_id + ")");
            plot.transform.GetChild(0).gameObject.active = true;
            plot.transform.GetChild(1).gameObject.active = true;
        }

    }

    public void Paint(int xPos, int yPos, int zPos)
    {
        Int32 x = Convert.ToInt32(xPos);
        Int32 y = Convert.ToInt32(yPos);
        Int32 z = Convert.ToInt32(zPos);
        position = new Vector3Int(x, y, z);

        for (int i = 0; i < 6; i++)
        {
            tilemap.SetTile(new Vector3Int(position.x + i, position.y, position.z), soil);
            tilemap.SetTile(new Vector3Int(position.x + i, position.y - 1, position.z), soil);
        }

    }

    public void Paint()
    {
        Int32 x = Convert.ToInt32(oldSign.transform.position.x - 0.5);
        Int32 y = Convert.ToInt32(oldSign.transform.position.y - 0.5);
        Int32 z = Convert.ToInt32(oldSign.transform.position.z);
        Debug.Log(x + " " + y + " " + z);
        position = new Vector3Int(x, y, z);

        for (int i = 0; i < 6; i++)
        {
            tilemap.SetTile(new Vector3Int(position.x + i, position.y, position.z), soil);
            tilemap.SetTile(new Vector3Int(position.x + i, position.y - 1, position.z), soil);
        }

    }

    private void Update()
    {
        //isUpgrade = false;
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null)
            {
                if (hit.collider.tag == "Sign")
                {
                    PopUp("Upgrade more slots cost " + userInfo.plot_own + "000 gold ! Continue ?");
                }
            }
        }

        if (isUpgrade)
        {
            

            // Add active plot
            for (int i = userInfo.plot_own - 1; i < userInfo.plot_own + 3; i++)
            {
                soilDetails[i].gameObject.SetActive(true);
            }

            userInfo.plot_own += 3;
            n.CommandAPI("upgrade-slot");

            if (oldSign.transform.position.x < 16 && oldSign.transform.position.y < 1)
            {
                if(userInfo.money >= 10000)
                {
                    Paint();
                    userInfo.money -= 10000;
                    newPosition = new Vector3(oldSign.transform.position.x + 7, oldSign.transform.position.y, oldSign.transform.position.z);
                    oldSign.transform.position = newPosition;
                }
                else
                {
                    notiCanvas.SetActive(true);
                    notiCanvas.transform.GetChild(0).GetChild(1).GetChild(1).GetChild(0).GetComponent<Text>().text = ("Sorry, you do not have enough money");
                }
                
            }
            else if (oldSign.transform.position.y < 1)
            {
                if (userInfo.money >= 20000)
                {
                    Paint();
                    userInfo.money -= 20000;
                    newPosition = new Vector3(oldSign.transform.position.x, oldSign.transform.position.y + 3, oldSign.transform.position.z);
                    oldSign.transform.position = newPosition;
                }
                else
                {
                    notiCanvas.SetActive(true);
                    notiCanvas.transform.GetChild(0).GetChild(1).GetChild(1).GetChild(0).GetComponent<Text>().text = ("Sorry, you do not have enough money");
                }
                
            }
            else
            {
                if (userInfo.money >= 40000)
                {
                    Paint();
                    userInfo.money -= 40000;
                    newPosition = new Vector3(oldSign.transform.position.x - 7, oldSign.transform.position.y, oldSign.transform.position.z);
                    oldSign.transform.position = newPosition;
                }
                else
                {
                    notiCanvas.SetActive(true);
                    notiCanvas.transform.GetChild(0).GetChild(1).GetChild(1).GetChild(0).GetComponent<Text>().text = ("Sorry, you do not have enough money");
                }
                
            }
            isUpgrade = false;
        }
    }

    public void PopUp(string text)
    {
        popUpText.GetComponent<UnityEngine.UI.Text>().text = text;
        popUpBox.SetActive(true);
    }

    public void Confirm()
    {
        isUpgrade = true;
        popUpBox.SetActive(false);
    }

    public void Cancel()
    {
        isUpgrade = false;
        popUpBox.SetActive(false);
    }

    
}
