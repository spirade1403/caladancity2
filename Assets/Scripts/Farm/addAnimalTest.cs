using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addAnimalTest : MonoBehaviour
{
    [SerializeField] FarmItemList farmItemList;
    

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i<farmItemList.ItemCount; i++)
        {
           // Vector3 possition = new Vector3(-3, 2, 0);
            Debug.Log(farmItemList.GetItem(i).prefabLink);
            for(int j = 1; j<= farmItemList.GetItem(i).itemQuantity; j++)
            {
                var spawnPosition = new Vector3(Random.Range(-8f, 1f), Random.Range(0f, 3f),0);
                var spawnRotation = Quaternion.Euler(0f, 0f, 0f);
                GameObject g = Resources.Load(farmItemList.GetItem(i).prefabLink) as GameObject;
                GameObject gObject = GameObject.Instantiate(g, spawnPosition, spawnRotation);
            }
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
