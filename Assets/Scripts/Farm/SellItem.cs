using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static NetworkManager;

public class SellItem : MonoBehaviour
{
    public Text itemCode;
    public Text quantity;
    public Text coin;
    [SerializeField] GameObject networkManager;
    public GameObject mainCanvas;
    public GameObject inventoryCanvas;
    public GameObject errorCanvas;

    // Start is called before the first frame update
    void Start()
    {
        // mainCanvas.SetActive(false);
    }

    public void Sell()
    {
        NetworkManager n = networkManager.GetComponent<NetworkManager>();
        List<UserInventoryJSON> itemList = n.playerInventory.data;
        int coinTotal = Int32.Parse(coin.text);

        //Get current item by id
        UserInventoryJSON itemDetails = this.GetItemByItemID(itemList, Int32.Parse(itemCode.text));

        print("Pressed sell " + itemDetails.item.name);
        GameObject currentGameObject = GameObject.Find("Inventory/UserInventory/Panel_Shop/Paper_Back/FarmProduct/Scroll View/Viewport/Content/" + itemDetails.item.name);


        //Validate quantity
        int inputtedQuantity;
        bool success = int.TryParse(quantity.text, out inputtedQuantity);
        if (success)
        {
            n.CommandSellInventory(itemDetails.item.id, Int32.Parse(quantity.text));
            print("coint before:" + coinTotal);

            if (Int32.Parse(quantity.text) > 0 && Int32.Parse(quantity.text) < itemDetails.quantity)
            {
                Debug.Log("quantity = " + quantity + "itemdetail quantity = " + itemDetails.quantity);

                coinTotal += (itemDetails.item.price * Int32.Parse(quantity.text));
                print("coint after:" + coinTotal);
                itemDetails.quantity -= Int32.Parse(quantity.text);
                currentGameObject.transform.GetChild(1).GetComponent<Text>().text = itemDetails.quantity + "";
            }
            else if (Int32.Parse(quantity.text) >= itemDetails.quantity)
            {
                coinTotal += (itemDetails.item.price * itemDetails.quantity);
                print("coint after:" + coinTotal);
                Debug.Log("Destroy here " + currentGameObject.name);
                Destroy(currentGameObject);
            }


            coin.text = coinTotal + "";
            quantity.text = "Hello"; 
            mainCanvas.SetActive(false);
            //   inventoryCanvas.SetActive(true);
        }
        else
        {
            quantity.text = "";

            errorCanvas.SetActive(true);
            errorCanvas.transform.GetChild(0).GetChild(1).GetChild(2).GetComponent<Text>().text = "Please input an integer number";
        }
    }

    public void HideCanvas()
    {
        mainCanvas.SetActive(false);
    }

    public UserInventoryJSON GetItemByItemID(List<UserInventoryJSON> itemList, int itemID)
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            if (itemList[i].id == itemID) return itemList[i];
        }

        return null;
    }
}
