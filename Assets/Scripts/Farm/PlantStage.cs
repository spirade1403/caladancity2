using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantStage : MonoBehaviour
{
    public GameObject stage2;
    public FarmItemList plant;

    float wateringTime;
    float finalStage;
    bool isDying = false;

    // Start is called before the first frame update
    void Start()
    {
        //wateringTime = plant.GetItem(0).dryTime;
        //finalStage = plant.GetItem(0).firstStage;
    }

    // Update is called once per frame
    void Update()
    {
        wateringTime -= Time.deltaTime;
        if (wateringTime < 0)
        {
            Sprite[] sprites = Resources.LoadAll<Sprite>("Weapon/Object-Farm-spritesheet");
            this.transform.GetComponent<SpriteRenderer>().sprite = sprites[81];
            this.transform.tag = "Dry";
            isDying = true;
        }
        else
        {
            if (finalStage <= 0)
            {
                if (this.transform.parent.childCount == 2 && isDying == false)
                {
                    GameObject plantStage2 = Instantiate(stage2, transform.position, transform.rotation, this.transform.parent);
                    Destroy(this.gameObject);
                }
            }
            else
            {
                finalStage -= Time.deltaTime;
            }
        }


        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null)
            {
                switch (PlayerPrefs.GetInt("itemNumber"))
                {
                    case 3://water
                        if (hit.collider.tag == "Dry")
                        {
                            Sprite[] sprites = Resources.LoadAll<Sprite>("Weapon/Object-Farm-spritesheet");
                            //this.transform.GetComponent<SpriteRenderer>().sprite = plant.GetItem(0).growingSprite;
                            this.transform.tag = "Untagged";
                            isDying = false;
                            //wateringTime = plant.GetItem(0).dryTime;
                        }
                        break;
                }
            }
        }
    }
}
