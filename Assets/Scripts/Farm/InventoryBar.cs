using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using static NetworkManager;

public class InventoryBar : MonoBehaviour
{
    [SerializeField] GameObject networkManager;
    GameObject player;
    GameObject gameObject;
    List<UserInventoryJSON> itemList;

    [SerializeField] GameObject itemTemplate;
    [SerializeField] Transform inventoryBar;
    public Animator animator;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        NetworkManager n = networkManager.GetComponent<NetworkManager>();

        yield return new WaitForSeconds(2.5f);
        itemList = n.playerInventory.data;

        player = GameObject.Find(PlayerPrefs.GetString("name") + "(Clone)");
        //animator = player.GetComponent<Animator>();
        addItemToInventory(itemList);
    }

    public void addItemToInventory(List<UserInventoryJSON> itemList)
    {
        int len = itemList.Count;
        for (int i = 0; i < len; i++)
        {
            if (itemList[i].item_type == 1 || itemList[i].item_type == 3)
            {
                gameObject = Instantiate(itemTemplate, inventoryBar);
                if (itemList[i].item.id == 3)
                {
                    UnityEngine.Object[] sprites;
                    sprites = Resources.LoadAll(itemList[i].item.sprite);
                    gameObject.transform.GetChild(2).GetComponent<Image>().sprite = (Sprite)sprites[15];
                }
                else if (itemList[i].item.id == 22)
                {
                    UnityEngine.Object[] sprites;
                    sprites = Resources.LoadAll(itemList[i].item.sprite);
                    gameObject.transform.GetChild(2).GetComponent<Image>().sprite = (Sprite)sprites[46];
                }
                else
                {
                    gameObject.transform.GetChild(2).GetComponent<Image>().sprite = Resources.Load<Sprite>(itemList[i].item.sprite);
                }


                RectTransform highlight = gameObject.transform.GetChild(0).GetComponent<RectTransform>();

                gameObject.transform.GetChild(3).GetComponent<Text>().text = itemList[i].item_id.ToString();

                gameObject.transform.GetChild(3).GetComponent<Text>().transform.localScale = Vector3.zero;
                highlight.transform.localScale = Vector3.zero;

                int itemCode = itemList[i].item_id;
                gameObject.name += itemCode;
                if (itemList[i].item_type == 3)
                {
                    gameObject.transform.GetChild(1).GetComponent<Text>().transform.localScale = Vector3.one;
                    gameObject.transform.GetChild(1).GetComponent<Text>().text = itemList[i].quantity.ToString();
                    gameObject.GetComponent<Button>().onClick.AddListener(delegate ()
                    {
                        PlayerPrefs.SetInt("itemNumber", itemCode);
                        unSelect();
                        
                        highlight.transform.localScale = Vector3.one;
                    });
                }
                else
                {
                    gameObject.transform.GetChild(1).GetComponent<Text>().transform.localScale = Vector3.zero;
                    gameObject.GetComponent<Button>().onClick.AddListener(delegate ()
                    {
                        PlayerPrefs.SetInt("itemNumber", itemCode);
                        unSelect();
                        animator.SetBool("isHoldingHoe", false);
                        animator.SetBool("isHoldingShovel", false);
                        animator.SetBool("isHoldingAxe", false);

                        switch (itemCode)
                        {
                            case 4:
                                animator.SetBool("isHoldingShovel", true);
                                break;
                            case 5:
                                animator.SetBool("isHoldingHoe", true);
                                break;
                        }
                        highlight.transform.localScale = Vector3.one;
                    });
                }
            }

        }
    }

    private void unSelect()
    {
        for (int i = 0; i < inventoryBar.childCount; i++)
        {
            inventoryBar.GetChild(i).transform.transform.GetChild(0).GetComponent<RectTransform>().transform.localScale = Vector3.zero;
        }
    }
    
}
