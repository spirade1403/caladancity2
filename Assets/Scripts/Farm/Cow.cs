using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static NetworkManager;

public class Cow : MonoBehaviour
{
    private bool moveRight = true;
    private float moveTime = 0;
    private float vRange = 0.003f;
    private int collectableCount = 3;
    GameObject gameObject;
    NetworkManager n;

    [SerializeField] public GameObject milk;
    public GameObject healthbar;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator.SetFloat("Speed", 1);
        StartCoroutine(Milk());
    }

    // Update is called once per frame
    void LateUpdate()
    {
        GameObject networkManager = GameObject.Find("Network Manager");
        NetworkManager n = networkManager.GetComponent<NetworkManager>();
        List<FarmAnimalJSON> farmItemList = n.farmAnimals.data;

        if (animator.GetFloat("Speed") != 0)
        {
            Move();
        }
        Die();

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null)
            {
                Debug.Log("Itemnumber: " + PlayerPrefs.GetInt("itemNumber"));
                switch (PlayerPrefs.GetInt("itemNumber"))
                {
                    case 2://harvest
                        if (hit.collider.tag == "Product")
                        {

                            //Destroy(hit.transform.gameObject); 
                            //GameObject cow = GameObject.Find("Cow(Clone)");
                            //Destroy(cow);

                            string id = hit.transform.GetChild(0).GetComponent<Text>().text;
                            print("harvest cow" + id);
                            AddProductToInventory(Int32.Parse(id));
                            Destroy(hit.transform.gameObject);
                            GameObject cow = GameObject.Find("Cow(Clone)" + id);
                            Destroy(cow);
                        }
                        break;
                    case 22: //feed
                        if (hit.collider.tag == "Animals")
                        {
                            int id = Convert.ToInt32(hit.transform.parent.GetChild(0).GetComponent<Text>().text);

                            for (int i = 0; i < farmItemList.Count; i++)
                            {
                                if (farmItemList[i].id == id)
                                {
                                    Debug.Log("COn nay la: " + id);
                                    if (this.transform.parent.GetChild(0).GetComponent<Text>().text == id.ToString())
                                    {
                                        GameObject currentInventoryItem = GameObject.Find("UI/MainGameUICanvas/UICanvasGroup/UIInventory/UIInventorySlot(Clone)" + PlayerPrefs.GetInt("itemNumber"));
                                        Debug.Log(currentInventoryItem.name);
                                        int currentQuantity = Int32.Parse(currentInventoryItem.transform.GetChild(1).GetComponent<Text>().text);
                                        Debug.Log("Quantity: " + currentQuantity);
                                        if (currentQuantity > 1)
                                        {
                                            currentInventoryItem.transform.GetChild(1).GetComponent<Text>().text = (currentQuantity - 1) + "";
                                        }
                                        else
                                        {
                                            Destroy(currentInventoryItem);
                                        }

                                        farmItemList[i].last_fed_time = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                        n.CommandFeedAnimal(id);
                                        break;
                                    }

                                }
                            }
                        }
                        break;
                }
            }
        }
    }

    void AddProductToInventory(int id)
    {
        GameObject networkManager = GameObject.Find("Network Manager");
        NetworkManager n = networkManager.GetComponent<NetworkManager>();
        n.CommandCollectAnimal(id);
        n.CommandAPI("user-inventory");
    }

    void Move()
    {
        moveTime -= Time.deltaTime;
        if (!moveRight)
        {
            GetComponent<Transform>().GetChild(0).localScale = new Vector3(1, 1, 1);
            GetComponent<Transform>().position += new Vector3(0.005f, vRange, 0);
        }
        else
        {
            GetComponent<Transform>().GetChild(0).localScale = new Vector3(-1, 1, 1);
            GetComponent<Transform>().position += new Vector3(-0.005f, vRange, 0);
        }

        if (moveTime <= 0)
        {

            moveTime = UnityEngine.Random.Range(1f, 10f);
            moveRight = !moveRight;
            vRange = -vRange;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        vRange = -vRange;
        if (collider.gameObject.tag == "Vertical")
        {
            moveRight = !moveRight;
        }
        else if (collider.gameObject.tag == "FullTray")
        {
            Eat();
        }
    }

    IEnumerator Milk()
    {
        yield return new WaitForSeconds(10f);
        GameObject Milk = Instantiate(milk, transform.position, transform.rotation);
        Milk.transform.GetChild(0).GetComponent<Text>().text = this.transform.parent.GetChild(0).GetComponent<Text>().text;
    }

    void Eat()
    {
        animator.SetBool("isEating", true);
        animator.SetFloat("Speed", 0);
        StartCoroutine(Eating());
    }
    IEnumerator Eating()
    {
        yield return new WaitForSeconds(2f);

        animator.SetBool("isEating", false);
        animator.SetFloat("Speed", 1);
    }

    void Die()
    {
        if (healthbar.transform.localScale.x <= 0)
        {
            //FarmItemDetail currentItem = animal.GetItemByItemNumber(this.transform.parent.GetChild(0).GetComponent<Text>().text);
            //animal.itemDetails.Remove(currentItem);
            Destroy(this.transform.parent.gameObject);
        }
    }
}
