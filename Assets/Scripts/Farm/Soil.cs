using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using static NetworkManager;

public class Soil : MonoBehaviour
{
    private bool isExist;
    private string plot;
    private int itemNumber;
    private DateTime startTime;
    private float growingTime;

    private NetworkManager n;
    List<FarmPlantJSON> listPlants;
    List<ItemJSON> seeds;
    List<ItemJSON> items;
    ItemJSON seed;
    IEnumerator Start()
    {
        yield return new WaitForSeconds(3f);
        GameObject networkManager = GameObject.Find("Network Manager");
        n = networkManager.GetComponent<NetworkManager>();
        listPlants = n.farmPlants.data;
        seeds = n.seeds.data;
        items = n.itemDetails.data;


        //PlayerPrefs.SetInt("itemNumber", -1);
        plot = this.transform.name;
        String plotId = Regex.Replace(plot, @"\D", "");

        if (listPlants.Count > 0)
        {
            for (int i = 0; i < listPlants.Count; i++)
            {
                if (listPlants[i].plot_id == Int32.Parse(plotId))
                {
                    itemNumber = i;
                    break;
                }
            }

            startTime = DateTime.ParseExact(listPlants[itemNumber].start_time, "yyyy-MM-dd HH:mm:ss", null);
            growingTime = listPlants[itemNumber].growing_time;
        }
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            plot = this.transform.name;
            String plotId = Regex.Replace(plot, @"\D", "");

            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

            if (hit.collider != null )
            {
                print(hit.collider.name);
                print(PlayerPrefs.GetInt("itemNumber"));
                print(hit.collider.tag);
                GameObject networkManager = GameObject.Find("Network Manager");

                switch (PlayerPrefs.GetInt("itemNumber"))
                {
                    case 4:
                        if (hit.collider.transform.tag == "Dry")
                        {
                            n = networkManager.GetComponent<NetworkManager>();
                            listPlants = n.farmPlants.data;
                            plotId = Regex.Replace(hit.collider.name, @"\D", "");
                            hit.collider.transform.GetChild(0).gameObject.active = false;
                            hit.collider.transform.GetChild(1).gameObject.active = false;
                            Debug.Log("Remove plot: " + plotId);
                            n.CommandRemovePlant(Int32.Parse(plotId), listPlants[itemNumber].item_id);
                            n.farmPlants.data.Remove(listPlants[itemNumber]);
                            hit.collider.transform.tag = "Soil";
                        }
                        break;
                    case 2://harvest
                        if (hit.collider.name == plot)
                        {
                            networkManager = GameObject.Find("Network Manager");
                            n = networkManager.GetComponent<NetworkManager>();
                            listPlants = n.farmPlants.data;
                            plotId = Regex.Replace(hit.collider.name, @"\D", "");
                            double timeLeft = growingTime - (DateTime.Now - startTime).TotalSeconds;
                            if (timeLeft <= 0)
                            {
                                Debug.Log("Harvest plot: " + plotId);
                                AddProductToInventory(listPlants[itemNumber]);
                                hit.transform.GetChild(0).gameObject.active = false;
                                hit.transform.GetChild(1).gameObject.active = false;
                                /*FarmItemDetail currentPlot = currentList.GetItemByPlotName(hit.collider.transform.name.ToString());
                                currentList.itemDetails.Remove(currentPlot);*/
                                n.farmPlants.data.Remove(listPlants[itemNumber]);
                                hit.collider.transform.tag = "Soil";
                            } 
                        }
                        break;
                    case 5://dig
                        if (hit.collider.tag == "Soil")
                        {
                            hit.collider.transform.GetChild(0).gameObject.active = true;
                        }
                        break;
                    case 9:
                    case 10:
                    case 11://plant
                        int j = 0;
                        networkManager = GameObject.Find("Network Manager");
                        n = networkManager.GetComponent<NetworkManager>();
                        listPlants = n.farmPlants.data;
                        if (hit.transform.childCount != 0)
                        {
                            print("hit coll" + hit.transform.name);
                            plotId = Regex.Replace(hit.transform.name, @"\D", "");
                            string plot = "Plot (" + hit.collider.transform.GetChild(1).GetChild(2).GetComponent<Text>().text + ")";
                            if (hit.transform.GetChild(0).gameObject.active)
                            {
                                Debug.Log("hello " + j);
                                isExist = false;

                                for (int i = 0; i < listPlants.Count; i++)
                                {
                                    print("plotExt:" + listPlants[i].plot_id);
                                    if (listPlants[i].plot_id == Int32.Parse(plotId))
                                    {
                                        isExist = true;
                                        break;
                                    }
                                }

                                print("isExt:" + isExist);

                               // if(!hit.transform.GetChild(1).gameObject.active)
                                if (!isExist)
                                {
                                    // Update inventory bar
                                    if (hit.collider.name == plot)
                                    {
                                        plotId = Regex.Replace(hit.transform.name, @"\D", "");
                                        Debug.Log("Plant on: " + plotId);
                                        AddToCurrentList(hit);
                                        n.CommandPlantSeed(Int32.Parse(plotId), PlayerPrefs.GetInt("itemNumber"));
                                        GameObject currentInventoryItem = GameObject.Find("UI/MainGameUICanvas/UICanvasGroup/UIInventory/UIInventorySlot(Clone)" + PlayerPrefs.GetInt("itemNumber"));
                                        Debug.Log(currentInventoryItem.name);
                                        int currentQuantity = Int32.Parse(currentInventoryItem.transform.GetChild(1).GetComponent<Text>().text);
                                        Debug.Log("Quantity: " + currentQuantity);
                                        if (currentQuantity > 1)
                                        {
                                            currentInventoryItem.transform.GetChild(1).GetComponent<Text>().text = (currentQuantity - 1) + "";
                                        }
                                        else
                                        {
                                            Destroy(currentInventoryItem);
                                        }

                                    }

                                    hit.collider.transform.GetChild(1).gameObject.active = true;
                                }                                
                            }
                        }                                          
                        break;
                }
            }
        }
    }

    void AddProductToInventory(FarmPlantJSON plant)
    {
        n.CommandPlantHarvest(plant.plot_id, plant.item_id);
    }

    public int generateID()
    {
        System.Random r = new System.Random();
        return r.Next(1000, 10000); //for ints
    }

    void AddToCurrentList(RaycastHit2D hit)
    {
        /*        FarmItemDetail itemDetail = new FarmItemDetail();
                FarmItemDetail currentPlant = plant.GetItemByItemNumber(PlayerPrefs.GetInt("itemNumber"));
                itemDetail.itemCode = currentPlant.itemCode;
                itemDetail.itemType = currentPlant.itemType;
                itemDetail.itemName = currentPlant.itemName;
                itemDetail.startTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                itemDetail.lastWaterTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                itemDetail.spriteLink = currentPlant.spriteLink;
                itemDetail.growingTime = currentPlant.growingTime;
                itemDetail.dyingTime = currentPlant.dyingTime;
                itemDetail.plot = hit.collider.name;
                currentList.itemDetails.Add(itemDetail);
        */
        ItemJSON product = null;

        for (int i = 0; i < seeds.Count; i++)
        {
            if (seeds[i].id == PlayerPrefs.GetInt("itemNumber"))
            {
                seed = seeds[i];
                break;
            }
        }

        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].id == Int32.Parse(seed.product_id))
            {
                product = items[i];
                break;
            }
        }

        int id = this.generateID();
        int userId = this.generateID();
        int slotId = Int32.Parse(Regex.Replace(hit.collider.name, @"\D", ""));
        int itemId = PlayerPrefs.GetInt("itemNumber");
        int productId = Int32.Parse(seed.product_id);
        string startTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        string lastFedTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        string spriteLink = seed.prefab_link;
        int growingTime = seed.time_to_grow;
        int dyingTime = seed.time_to_live;
        int status = 1;
        print("dying time:" + dyingTime);
        print("prefab link:" + spriteLink);
        FarmPlantJSON plant = new FarmPlantJSON(id,userId, slotId, itemId, productId, startTime, growingTime, spriteLink, lastFedTime, dyingTime, status, seed, product);
        n.farmPlants.data.Add(plant);
    }

}
