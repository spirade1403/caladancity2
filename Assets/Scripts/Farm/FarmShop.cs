using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static NetworkManager;

public class FarmShop : MonoBehaviour
{
    GameObject gameObject;
    [SerializeField] GameObject itemTemplate;
    [SerializeField] Transform shopScrollView;
    Button buyBtn;

    private ItemListJSON seedList;
    private ItemListJSON  animalList;
    private UserInfoJSON playerInfo;
    [SerializeField] GameObject networkManager;
    [SerializeField] Text coinField;
    [SerializeField] string shopType;
    public GameObject errorCanvas;

    IEnumerator Start()
    {
        NetworkManager n = networkManager.GetComponent<NetworkManager>();

        yield return new WaitForSeconds(2);
        seedList = n.seeds;
        animalList = n.animals;
        playerInfo = n.userInfo;

        switch (shopType)
        {
            case "seed":
                SeedShop(seedList);
                break;
            case "animal":
                AnimalShop(animalList);
                break;
        }

    }


    private void SeedShop(ItemListJSON seedList)
    {
        NetworkManager n = networkManager.GetComponent<NetworkManager>();
        Debug.Log("Seed: " + seedList.data.Count);

        int len = seedList.data.Count;
        coinField.text = playerInfo.money.ToString();

        for (int i = 0; i < len; i++)
        {
            gameObject = Instantiate(itemTemplate, shopScrollView);
            if (seedList.data[i].id == 22)
            {
                UnityEngine.Object[] sprites;
                sprites = Resources.LoadAll(seedList.data[i].sprite);
                gameObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = (Sprite)sprites[46];
            }
            else
            {
                gameObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(seedList.data[i].sprite);
            }

            gameObject.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = seedList.data[i].price.ToString();
            gameObject.transform.GetChild(0).GetChild(3).GetComponent<Text>().text = seedList.data[i].name.ToString();
            Button btnBuy = gameObject.transform.GetChild(0).GetChild(2).GetComponent<Button>();

            NetworkManager.ItemJSON itemDetail = seedList.data[i];

           
            int price = seedList.data[i].price;

            btnBuy.onClick.AddListener(delegate ()
            {
                int coin = playerInfo.money;
                // buyBtn.transform.GetChild(0).GetComponent<Text>().text = "Onclick " + i;
                if (coin >= price)
                {
                    playerInfo.money -= price;
                    coinField.text = playerInfo.money.ToString();
                    n.CommandPurchaseItem(itemDetail.id);
                }
                else
                {
                    errorCanvas.SetActive(true);
                    errorCanvas.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Text>().text = "Sorry, you don't have enough money";
                    Debug.Log("Khong du tien");
                }

            });
        }
        //  Destroy(itemTemplate);
    }

    private void AnimalShop(ItemListJSON animalList)
    {
        Debug.Log("Animal");
        // itemTemplate = shopScrollView.GetChild(0).gameObject;
        NetworkManager n = networkManager.GetComponent<NetworkManager>();
        Debug.Log("Animal:" + animalList.data.Count);
        int len = animalList.data.Count;

        coinField.text = playerInfo.money.ToString();


        for (int i = 0; i < len; i++)
        {
            gameObject = Instantiate(itemTemplate, shopScrollView);
            gameObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(animalList.data[i].sprite);
            gameObject.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = animalList.data[i].price.ToString();
            gameObject.transform.GetChild(0).GetChild(3).GetComponent<Text>().text = animalList.data[i].name.ToString();
            Button btnBuy = gameObject.transform.GetChild(0).GetChild(2).GetComponent<Button>();

            NetworkManager.ItemJSON itemDetail = animalList.data[i];
           
            int price = animalList.data[i].price;

            btnBuy.onClick.AddListener(delegate ()
            {
                int coin = playerInfo.money;
                //TODO : load total animals players having from DB
                int totalAnimals = 0;
                if (totalAnimals >= 8)
                {
                    errorCanvas.SetActive(true);
                    errorCanvas.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Text>().text = "Sorry, you don't have enough slots";
                    Debug.Log("khong cho mua");
                }
                else
                {
                    if (coin >= price)
                    {
                        playerInfo.money -= price;
                        coinField.text = playerInfo.money.ToString();
                        errorCanvas.SetActive(true);
                        errorCanvas.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Text>().text = "Buy Successfully";
                        n.CommandPurchaseItem(itemDetail.id);
                    }
                    else
                    {
                        errorCanvas.SetActive(true);
                        errorCanvas.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Text>().text = "Sorry, you don't have enough money";
                        Debug.Log("Khong du tien");
                    }
                }

            });
        }
        
        //  Destroy(itemTemplate);
    }
    public void Test(int i)
    {
        
        Debug.Log("onclick: " + i);

    }


}
