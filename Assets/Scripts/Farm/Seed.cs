using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using static NetworkManager;

public class Seed : MonoBehaviour
{
    float growingTime;
    bool isFirstStage;
    private DateTime startTime;
    private string plot;

    private int itemNumber;
    public GameObject healthbar;

    private NetworkManager n;
    List<FarmPlantJSON> listPlants;
    FarmPlantJSON currentPlant;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(2f);
        Init();
    }

    void OnDisable()
    {
        //Init();
    }

    void Init()
    {
        isFirstStage = true;


        GameObject networkManager = GameObject.Find("Network Manager");
        n = networkManager.GetComponent<NetworkManager>();
        listPlants = n.farmPlants.data;
        print("plant count:" +listPlants.Count);
        if (this.transform.childCount == 3)
        {
            plot = this.transform.parent.name;
            String plotId = Regex.Replace(plot, @"\D", "");

            for (int i = 0; i < listPlants.Count; i++)
            {
                if (listPlants[i].plot_id == Int32.Parse(plotId))
                {
                    itemNumber = i;
                    print("itemNumb:" +itemNumber);
                    break;
                }
            }
        }


        startTime = DateTime.ParseExact(listPlants[itemNumber].start_time, "yyyy-MM-dd HH:mm:ss", null);
        growingTime = listPlants[itemNumber].growing_time / 2;
        currentPlant = listPlants[itemNumber];

        if (currentPlant != null)
        {
            print(currentPlant.sprite_link);
            this.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(currentPlant.sprite_link + "seed");
        }
    }

    // Update is called once per frame
    void Update()
    {
        GameObject networkManager = GameObject.Find("Network Manager");
        n = networkManager.GetComponent<NetworkManager>();
        listPlants = n.farmPlants.data;

        if (this.transform.childCount == 3)
        {
            plot = this.transform.parent.name;
            String plotId = Regex.Replace(plot, @"\D", "");

            for (int i = 0; i < listPlants.Count; i++)
            {
                if (listPlants[i].plot_id == Int32.Parse(plotId))
                {
                    itemNumber = i;
                    break;
                }
            }
        }

        print("plant count:" + listPlants.Count);
        print("itemNumber:" + itemNumber);
        startTime = DateTime.ParseExact(listPlants[itemNumber].start_time, "yyyy-MM-dd HH:mm:ss", null);
        growingTime = listPlants[itemNumber].growing_time / 2;
        currentPlant = listPlants[itemNumber];

        if (currentPlant != null)
        {
            this.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(currentPlant.sprite_link + "seed");
        }

        if (currentPlant != null)
        {
            TimeSpan timePass = DateTime.Now - startTime;
            double totalSecondsLeft = timePass.TotalSeconds;
            if (isFirstStage)
            {
                if (totalSecondsLeft >= growingTime)
                {
                    this.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(currentPlant.sprite_link + "stage1");
                    this.transform.GetChild(1).localScale = new Vector3(3, 3, 3);
                    isFirstStage = false;
                    growingTime *= 2;
                }
            }
            else
            {
                if (totalSecondsLeft >= growingTime)
                {
                    this.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(currentPlant.sprite_link + "stage2");
                    this.transform.GetChild(1).localScale = new Vector3(3, 3, 3);
                }
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null)
            {
                switch (PlayerPrefs.GetInt("itemNumber"))
                {
                    case 3://water
                        if (hit.collider.tag == "Soil")
                        {
                            plot = hit.transform.name;
                            String plotId = Regex.Replace(plot, @"\D", "");

                            for (int i = 0; i < listPlants.Count; i++)
                            {
                                if (listPlants[i].plot_id == Int32.Parse(plotId))
                                {
                                    listPlants[i].last_fed_time = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                    n.CommandPlantWater(listPlants[i].plot_id, listPlants[i].item_id);
                                    break;
                                }
                            }                            
                        }
                        break;
                }
            }
        }

        Die();
    }

    void Die()
    {
        if (healthbar.transform.localScale.x <= 0)
        {
            Sprite[] sprites = Resources.LoadAll<Sprite>("Weapon/Object-Farm-spritesheet");
            this.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = sprites[81];
            this.transform.parent.tag = "Dry";
            this.transform.GetChild(1).localScale = new Vector3(1, 1, 1);
        }
    }


}
