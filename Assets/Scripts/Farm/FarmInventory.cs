using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using static NetworkManager;

public class FarmInventory : MonoBehaviour
{
    public GameObject mainCanvas;
    GameObject gameObject;

    [SerializeField] GameObject networkManager;
    [SerializeField] public GameObject itemTemplate;
    [SerializeField] public Transform content;

    public string type;
    private Transform dragTransform;
    bool isExist = false;
    public GameObject canvasSell;
    public Text itemCodeText;
   // public Text selectedTabName;

    void Start()
    {
        DestroyAll();
        switch (type)
        {
            case "farmProduct":
                farmProduct();
                break;
            case "clothes":
                clothes();
                break;
        }

    }

    public void DisplayCanvas()
    {
        mainCanvas.SetActive(true);
        NetworkManager n = networkManager.GetComponent<NetworkManager>();
        n.CommandAPI("user-inventory");
        n.CommandAPI("user-inventory");

        DestroyAll();
        switch (type)
        {
            case "farmProduct":
                DestroyAll();
                farmProduct();
                break;
            case "clothes":
                DestroyAll();
                clothes();
                break;
        }
    }

    IEnumerator ButtonDelay()
    {
        Debug.Log(Time.time);
        yield return new WaitForSeconds(1f);
    }

    public void HideCanvas()
    {
        DestroyAll();
        mainCanvas.SetActive(false);
    }

    private void farmProduct()
    {
        //   if (isExist) return;
        DestroyAll();
        NetworkManager n = networkManager.GetComponent<NetworkManager>();
        n.CommandAPI("user-inventory");
        List<UserInventoryJSON> itemList = n.playerInventory.data;

        for (int i = 0; i < itemList.Count; i++)
        {
            if (itemList[i].item_type == 9)
            {
                print("Render " + itemList[i].id);
                gameObject = Instantiate(itemTemplate, content);
                gameObject.name = itemList[i].item.name;
                if (itemList[i].item.id == 19)
                {
                    UnityEngine.Object[] sprites;
                    sprites = Resources.LoadAll(itemList[i].item.sprite);
                    gameObject.transform.GetChild(0).GetComponent<Image>().sprite = (Sprite)sprites[15];
                }
                else if (itemList[i].item.id == 20)
                {
                    UnityEngine.Object[] sprites;
                    sprites = Resources.LoadAll(itemList[i].item.sprite);
                    gameObject.transform.GetChild(0).GetComponent<Image>().sprite = (Sprite)sprites[57];
                }
                else
                {
                    gameObject.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(itemList[i].item.sprite);
                }

                gameObject.transform.GetChild(1).GetComponent<Text>().text = itemList[i].quantity.ToString();

                int itemCode = itemList[i].id;

                gameObject.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Sell";
                gameObject.transform.GetChild(3).GetComponent<Text>().text = itemCode.ToString();
                gameObject.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate ()
                {
                    print("Selling " + itemCode);
                    itemCodeText.text = itemCode.ToString();
                    showSellCanvas();
                    // HideCanvas();
                });
            }
        }
        isExist = true;
    }

    public void sellItem()
    {

    }
    public void showSellCanvas()
    {
        canvasSell.SetActive(true);
    }

    public void hidSellCanvas()
    {
        canvasSell.SetActive(false);
    }

    private void clothes()
    {
        //   if (isExist) return;
        DestroyAll();
        NetworkManager n = networkManager.GetComponent<NetworkManager>();

        List<UserInventoryJSON> itemList = n.playerInventory.data;
        print("item count:" + itemList.Count);

        for (int i = 0; i < itemList.Count; i++)
        {
            print("Render " + itemList[i].id);
            if (itemList[i].item_type == 4 || itemList[i].item_type == 5
                || itemList[i].item_type == 6 || itemList[i].item_type == 7 || itemList[i].item_type == 8)
            {
                gameObject = Instantiate(itemTemplate, content);
                if (itemList[i].item_type == 8)
                {
                    gameObject.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(itemList[i].item.sprite + "/body-idle");
                }
                else
                {
                    gameObject.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(itemList[i].item.sprite);
                }

                gameObject.transform.GetChild(1).GetComponent<Text>().text = itemList[i].quantity.ToString();
                gameObject.transform.GetChild(1).GetComponent<Text>().transform.localScale = Vector3.zero;
                int itemId = itemList[i].item.id;


                int itemType = itemList[i].item_type;
                gameObject.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Change";

                string bodySprite = itemList[i].item.sprite;
                /*string frontArmSprite = itemList.GetItem(i).frontArmSprite;
                string backArmSprite = itemList.GetItem(i).backArmSprite;*/

                gameObject.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate ()
                {
                    Debug.Log("Im here");
                    Debug.Log(bodySprite);
                    n.CommandEquipInventory(itemId);
                    n.CommandAPI("user-info");
                    n.CommandAPI("user-info");
                });
            }

        }
    }

    private void DestroyAll()
    {
        GameObject[] listitem;
        listitem = GameObject.FindGameObjectsWithTag("InventorySlot");
        foreach (GameObject g in listitem)
        {
            Destroy(g);
        }

    }
}
