using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static NetworkManager;
using UnityEngine.UI;

public class AddAnimalToCote : MonoBehaviour
{

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(3f);
        GameObject networkManager = GameObject.Find("Network Manager");
        print(networkManager.name);
        NetworkManager n = networkManager.GetComponent<NetworkManager>();
        List<FarmAnimalJSON> farmItemList = n.farmAnimals.data;
        print(farmItemList.Count);

        for (int i = 0; i < farmItemList.Count; i++)
        {
            // Vector3 possition = new Vector3(-3, 2, 0);
            var spawnPosition = new Vector3(Random.Range(-19f, -11f), Random.Range(-7f, -4f), 0);
            var spawnRotation = Quaternion.Euler(0f, 0f, 0f);
            if (farmItemList[i].item.id == 7)
            {
                spawnPosition = new Vector3(Random.Range(24f, 33f), Random.Range(-2f, 1f), 0);
                spawnRotation = Quaternion.Euler(0f, 0f, 0f);
            }
            print("prefab link");
            print(farmItemList[i].prefab_link);
            GameObject g = Resources.Load(farmItemList[i].prefab_link) as GameObject;
            GameObject gObject = GameObject.Instantiate(g, spawnPosition, spawnRotation);
            gObject.transform.GetChild(0).GetComponent<Text>().text = farmItemList[i].id.ToString();
            gObject.transform.GetChild(0).GetComponent<Text>().transform.localScale = Vector3.zero;
            //SceneManager.MoveGameObjectToScene(gObject, SceneManager.GetSceneByName("scene1_Farm"));
            print("spawning " + farmItemList[i].id);
        }
    }
}
