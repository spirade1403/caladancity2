   using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FarmShopSocket : MonoBehaviour
{
    GameObject gameObject;
    [SerializeField] GameObject itemTemplate;
    [SerializeField] Transform shopScrollView;
    [SerializeField] GameObject networkManager; 
    [SerializeField] Text coinField;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        NetworkManager.ItemListJSON itemList;
        NetworkManager n = networkManager.GetComponent<NetworkManager>();

        n.CommandAPI("item-list");
        n.CommandAPI("user-info");
        yield return new WaitForSeconds(2);

        // itemTemplate = shopScrollView.GetChild(0).gameObject;
        NetworkManager.UserInfoJSON player = n.userInfo;
        itemList = n.itemDetails;

        int len = itemList.data.Count;
        Debug.Log("count" + len);

        coinField.text = player.money.ToString();
        for (int i = 0; i < len; i++)
        {
            gameObject = Instantiate(itemTemplate, shopScrollView);
            gameObject.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(itemList.data[i].sprite);
            gameObject.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = itemList.data[i].price.ToString();
            Button btnBuy = gameObject.transform.GetChild(2).GetComponent<Button>();
            NetworkManager.ItemJSON itemDetail = itemList.data[i];
            Debug.Log(itemDetail);

            int coin = player.money;
            int price = itemList.data[i].price;
            btnBuy.onClick.AddListener(delegate ()
            {
                // buyBtn.transform.GetChild(0).GetComponent<Text>().text = "Onclick " + i;
                if (coin >= price)
                {
                    player.money -= price;
                    coinField.text = player.money.ToString();
                    n.CommandPurchaseItem(itemDetail.id);
                }
                else
                {
                    Debug.Log("Khong du tien");
                }


            });
        }
      //  Destroy(itemTemplate);

    }

    public void Test(int i)
    {
        
        Debug.Log("onclick: " + i);

    }


}
