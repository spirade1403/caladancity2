using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tray : MonoBehaviour
{
    private int count;

    public GameObject emptyTray;
    public GameObject fullTray;

    void Start()
    {
        count = 0;
        PlayerPrefs.SetInt("itemNumber", -1);
    }

    void Update()
    {
        if(count >= 5)
        {
            Destroy(this.gameObject);
            Instantiate(emptyTray, transform.position, transform.rotation);
        }

        if (Input.GetMouseButtonDown(0))
        {
            
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

            if (hit.collider != null)
            {
                switch (PlayerPrefs.GetInt("itemNumber"))
                {                    
                    case 4://feed
                        
                        if (hit.collider.tag.Equals("EmptyTray"))
                        {
                            Destroy(this.gameObject);
                            Instantiate(fullTray, transform.position, transform.rotation);
                        }
                        break;
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (collider.gameObject.tag == "Animals")
        {
            count++;
        }
    }

}
