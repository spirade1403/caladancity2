using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class FarmCurent
{
    public int itemCode;
    public ItemType itemType;
    public string itemDescription;
    public string itemName;
    public int itemPrice;
    public int itemQuantity;
    public int health;
    public string status;
    public Sprite seedSprite;
    public Sprite growingSprite;
    public Sprite grownSprite;
    public Sprite productSprite;
    public string prefabLink;
}
