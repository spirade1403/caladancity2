using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableFarmInventory : MonoBehaviour
{
    public GameObject mainCanvas;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject collisionGameObject = collision.gameObject;
        if (collisionGameObject.tag == "Player")
            mainCanvas.SetActive(true);
    }

    public void DisableCanvas()
    {
        mainCanvas.SetActive(false);
    }

}
