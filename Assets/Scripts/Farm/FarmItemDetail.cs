using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class FarmItemDetail 
{
    public int itemCode;
    public ItemType itemType;
    public string itemName;
    public int itemPrice;
    public int itemQuantity;
    public string startTime;
    public string lastWaterTime;
    public float growingTime;
    public float dyingTime;
    public string spriteLink;
    public string prefabLink;
    public string plot;
}
