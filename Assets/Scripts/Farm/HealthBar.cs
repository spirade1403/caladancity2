using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using static NetworkManager;

public class HealthBar : MonoBehaviour
{

    public GameObject bar;
    private NetworkManager n;
    List<FarmPlantJSON> listPlants;
    List<FarmAnimalJSON> listAnimals;
    GameObject networkManager;
    private float dyingTime;
    private DateTime lastWaterTime;
    private string plot;
    private int itemNumber;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        bar.transform.localScale = new Vector3(1f, 1f, 1f);
        yield return new WaitForSeconds(2f);
        networkManager = GameObject.Find("Network Manager");
        NetworkManager n = networkManager.GetComponent<NetworkManager>();
        listPlants = n.farmPlants.data;
        listAnimals = n.farmAnimals.data;
    }

    // Update is called once per frame
    void Update()
    {
        networkManager = GameObject.Find("Network Manager");
        NetworkManager n = networkManager.GetComponent<NetworkManager>();
        listPlants = n.farmPlants.data;
        listAnimals = n.farmAnimals.data;

        if (this.transform.parent.tag == "Plant")
        {
            plot = this.transform.parent.parent.name;
            String plotId = Regex.Replace(plot, @"\D", "");

            for (int i = 0; i < listPlants.Count; i++)
            {
                if (listPlants[i].plot_id == Int32.Parse(plotId))
                {
                    itemNumber = i;
                    break;
                }
            }
            print("plant count:" + listPlants.Count);
            print("itemNumber:" + itemNumber);

            if (listPlants[itemNumber].last_fed_time != null)
            {
                lastWaterTime = DateTime.ParseExact(listPlants[itemNumber].last_fed_time, "yyyy-MM-dd HH:mm:ss", null);
                dyingTime = listPlants[itemNumber].dying_time;
            }
        }
        else if (this.transform.parent.tag == "Animals")
        {
            int id = Convert.ToInt32(this.transform.parent.parent.GetChild(0).GetComponent<Text>().text);
            for (int i = 0; i < listAnimals.Count; i++)
            {
                if (listAnimals[i].id == id)
                {
                    itemNumber = i;
                    break;
                }
            }

            if (listAnimals[itemNumber].last_fed_time != null)
            {
                lastWaterTime = DateTime.ParseExact(listAnimals[itemNumber].last_fed_time, "yyyy-MM-dd HH:mm:ss", null);
                dyingTime = listAnimals[itemNumber].dying_time;
            }
        }

        TimeSpan timePass = DateTime.Now - lastWaterTime;
        double totalSecondsLeft = timePass.TotalSeconds;

        float percentage = 1 - ((float)totalSecondsLeft / dyingTime);

        if (percentage >= 0)
        {
            bar.transform.localScale = new Vector3(percentage, 1f, 1f);
        }
        else
        {
            bar.transform.localScale = new Vector3(0f, 1f, 1f);
        }
    }
}
