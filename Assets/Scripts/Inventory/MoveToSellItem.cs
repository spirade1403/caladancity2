using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveToSellItem : MonoBehaviour
{
    public Text itemCodeText;
    // Start is called before the first frame update
    void Start()
    {
    }

    public void goToSellItem()
    {
        GameObject.Find("ItemCode").transform.GetComponent<Text>().text = itemCodeText.text;
        //GameObject.Find("SellItem").transform.GetComponent<GameObject>().SetActive(true);
        FindRecursive(GameObject.Find("CanvasSellItem").transform.GetComponent<GameObject>(), "SellItem").SetActive(true);
    }

    
    private static GameObject FindRecursive(GameObject obj, string search)
    {
        GameObject result = null;
        foreach (Transform child in obj.transform)
        {
            if (child.name.Equals(search)) return child.gameObject;

            result = FindRecursive(child.gameObject, search);

            if (result) break;
        }

        return result;
    }
}
