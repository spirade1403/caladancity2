using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerDetail 
{
    public int userID;
    public string username;
    public int friendlyStat;
    public int coin;
    public int gem;
    public int level;
    public string faceSprite;
    public string itemHolding;
    public string faceDecorationSprite;
    public string hatSprite;
    public string hairSprite;
    public string bodySprite;
    public string frontArmSprite;
    public string backArmSprite;
    public string backHairSprite;
    public Sprite avatar;
    public int numberOfPlot;
}
