using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadFriendRequest : MonoBehaviour
{
    public GameObject mainCanvas;
    GameObject gameObject;
    [SerializeField] public GameObject friendTemplate;
    [SerializeField] public GameObject friendRequestTemplate;
    [SerializeField] public GameObject leaderBoardTemplate;
    [SerializeField] public Transform FriendGrid;
    [SerializeField] public Transform FriendRequestGrid;
    [SerializeField] public Transform LeaderBoardGrid;
    [SerializeField] PlayerList friendRequestList;
    [SerializeField] PlayerList friendList;
    [SerializeField] PlayerList leaderBoardList;
    [SerializeField] string type;
    [SerializeField] GameObject profileCanvas;
    // Start is called before the first frame update
    void Start()
    {
        switch (type)
        {
            case "friendList":
                ShowFriendList();
                break;
            case "friendRequestList":
                ShowFriendRequestList();
                break;
            case "leaderBoard":
                ShowLeaderBoardList();
                break;
        }
    }

    public void ShowCanvas()
    {
        mainCanvas.SetActive(true);
    }

    public void HideCanvas()
    {
        mainCanvas.SetActive(false);
    }

    public void ShowFriendList()
    {
        for (int i = 0; i < friendList.ItemCount; i++)
        {
            gameObject = Instantiate(friendTemplate, FriendGrid);
            gameObject.transform.GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>("icon_512_1_04");
            gameObject.transform.GetChild(0).GetComponent<Text>().text = friendList.GetItem(i).username.ToString();
            gameObject.transform.GetChild(4).GetComponent<Text>().text = friendList.GetItem(i).userID.ToString();
            gameObject.transform.GetChild(4).GetComponent<Text>().transform.localScale = Vector3.zero;
            gameObject.name += i + "";
            string name = gameObject.name;
            int userID = friendList.GetItem(i).userID;
            PlayerDetail currentPlayer = friendList.GetItem(i);

            // Button message
            gameObject.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate ()
            {
                
            });

            // Button delete
            gameObject.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(delegate ()
            {
                friendList.playerDetails.Remove(currentPlayer);
                Destroy(GameObject.Find("Friends/Window/Canvas/List/FriendList/Viewport/Grid/" + name));
            });

            // Button view profile 
            gameObject.transform.GetChild(5).GetComponent<Button>().onClick.AddListener(delegate ()
            {
                PlayerDetail playerDetail = currentPlayer;
                profileCanvas.GetComponent<RectTransform>().transform.localScale = Vector3.one;

                //Player Name
                profileCanvas.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = playerDetail.username;
                //username.text = playerDetail.username;

                //Friendly stat
                profileCanvas.transform.GetChild(1).GetChild(1).GetComponent<Slider>().maxValue = 100;
                profileCanvas.transform.GetChild(1).GetChild(1).GetComponent<Slider>().value = playerDetail.friendlyStat;
                profileCanvas.transform.GetChild(1).GetChild(1).GetComponent<Slider>().interactable = false;

                //Money
                profileCanvas.transform.GetChild(1).GetChild(0).GetChild(0).GetChild(0).transform.GetComponent<Text>().text = playerDetail.coin.ToString();
                profileCanvas.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).transform.GetComponent<Text>().text = playerDetail.gem.ToString();
                profileCanvas.transform.GetChild(1).GetChild(0).GetChild(2).GetChild(1).transform.GetComponent<Text>().text = playerDetail.level.ToString();

                //Player outfit
                if (playerDetail.faceSprite != "")
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).transform.GetComponent<Image>().sprite
                   = Resources.Load<Sprite>(playerDetail.faceSprite);
                else
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).transform.GetComponent<Image>().enabled = false;

                if (playerDetail.faceDecorationSprite != "")
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).transform.GetComponent<Image>().sprite
                    = Resources.Load<Sprite>(playerDetail.faceDecorationSprite);
                else
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).transform.GetComponent<Image>().enabled = false;

                if (playerDetail.hairSprite != "")
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(4).transform.GetComponent<Image>().sprite
                    = Resources.Load<Sprite>(playerDetail.hairSprite);
                else
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(4).transform.GetComponent<Image>().enabled = false;

                if (playerDetail.hatSprite != "")
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(5).transform.GetComponent<Image>().sprite
                    = Resources.Load<Sprite>(playerDetail.hatSprite);
                else
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(5).transform.GetComponent<Image>().enabled = false;

                if (playerDetail.bodySprite != "")
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(1).transform.GetComponent<Image>().sprite
                   = Resources.Load<Sprite>(playerDetail.bodySprite);
                else
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(1).transform.GetComponent<Image>().enabled = false;

                if (playerDetail.frontArmSprite != "")
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(3).transform.GetComponent<Image>().sprite
                   = Resources.Load<Sprite>(playerDetail.frontArmSprite);
                else
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(3).transform.GetComponent<Image>().enabled = false;

                if (playerDetail.backHairSprite != "")
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).transform.GetComponent<Image>().sprite
                 = Resources.Load<Sprite>(playerDetail.backHairSprite);
                else
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).transform.GetComponent<Image>().enabled = false;

                if (playerDetail.backArmSprite != "")
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).transform.GetComponent<Image>().sprite
                = Resources.Load<Sprite>(playerDetail.backArmSprite);
                else
                    profileCanvas.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).transform.GetComponent<Image>().enabled = false;


                //Buton Add Friend
                //if (player1FriendRequestList.GetUserByID(0) != null)
                //{
                //    profileCanvas.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = "Sent friend request";
                //}
                //else if (currentPlayerFriendList.GetUserByID(id) != null)
                //{
                //    profileCanvas.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = "Friend";
                //}
                //else
                //{
                //    profileCanvas.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = "Add friend";
                //}
                //profileCanvas.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(delegate ()
                //{
                //    profileCanvas.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = "Sent friend request";
                //    player1FriendRequestList.playerDetails.Add(playerList.GetItem(0));
                //});
            
            });
        }
    }

    public void ShowFriendRequestList()
    {
        for (int i = 0; i < friendRequestList.ItemCount; i++)
        {
            gameObject = Instantiate(friendRequestTemplate, FriendRequestGrid);
            gameObject.transform.GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>("icon_512_1_04");
            gameObject.transform.GetChild(0).GetComponent<Text>().text = friendRequestList.GetItem(i).username.ToString();
            gameObject.transform.GetChild(4).GetComponent<Text>().text = friendRequestList.GetItem(i).userID.ToString();
            gameObject.transform.GetChild(4).GetComponent<Text>().transform.localScale = Vector3.zero;
            gameObject.name += i + "";
            string name = gameObject.name;
            int userID = friendRequestList.GetItem(i).userID;
            PlayerDetail currentPlayer = friendRequestList.GetItem(i);

            //Button Accept
            gameObject.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate ()
            {
                // Add to scriptable object
                friendList.playerDetails.Add(currentPlayer);

                // Add to Canvas
                gameObject = Instantiate(friendTemplate, FriendGrid);
                gameObject.transform.GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>("icon_512_1_04");
                gameObject.transform.GetChild(0).GetComponent<Text>().text = currentPlayer.username.ToString();
                gameObject.transform.GetChild(4).GetComponent<Text>().text = currentPlayer.userID.ToString();
                gameObject.transform.GetChild(4).GetComponent<Text>().transform.localScale = Vector3.zero;

                // Remove from friend request list
                friendRequestList.playerDetails.Remove(currentPlayer);

                // Remove from Canvas
                Destroy(GameObject.Find("Friends/Window/Canvas/List/FriendRequestList/Viewport/Grid/" + name));
            });

            //Button Delete
            gameObject.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(delegate ()
            {
                friendRequestList.playerDetails.Remove(currentPlayer);
                Destroy(GameObject.Find("Friends/Window/Canvas/List/FriendRequestList/Viewport/Grid/" + name));
            });

        }
    }

    public void ShowLeaderBoardList()
    {
        for (int i = 0; i < leaderBoardList.ItemCount; i++)
        {
            gameObject = Instantiate(leaderBoardTemplate, LeaderBoardGrid);
            gameObject.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = (i+1)+"";
            gameObject.transform.GetChild(0).GetComponent<Text>().text = leaderBoardList.GetItem(i).username.ToString();
            gameObject.transform.GetChild(3).GetComponent<Text>().text = leaderBoardList.GetItem(i).userID.ToString();
            gameObject.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = leaderBoardList.GetItem(i).coin.ToString();
            gameObject.name += (i) + "";
           

           

        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
