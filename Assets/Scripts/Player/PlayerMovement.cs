using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public float moveSpeed = 5f;
    public Rigidbody2D rb;

    Vector2 movement;

    public Animator animator;

    private JoyStickManager joystickManager;

    private void Start()
    {
        joystickManager = GameObject.Find("imgJoystickBg").GetComponent<JoyStickManager>();
    }

    // Update is called once per frame
    void Update()
    {

        movement.x = joystickManager.inputHorizontal();
        movement.y = joystickManager.inputVertical();

        bool flipped = movement.x < 0;
        if(movement.x < 0)
        {
            rb.transform.localScale = new Vector3(1,1,1);
        } 
        else if(movement.x > 0)
        {
            rb.transform.localScale = new Vector3(-1, 1, 1);
        }

        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
    }
}

