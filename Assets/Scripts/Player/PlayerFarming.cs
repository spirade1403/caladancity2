using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerFarming : MonoBehaviour
{
    // Start is called before the first frame update
    MouseInput mouseInput;
    void Awake()
    {
        mouseInput = new MouseInput();
        mouseInput.Enable();
    }

    //void OnEnable()
    //{
        
    //}

    //void OnDisable()
    //{
    //    mouseInput.Disable();
    //}

    void Start()
    {
        mouseInput.Farm.MouseClick.performed += ctx => MouseClick();
    }

    private void MouseClick()
    {
        Vector2 mousePosition = mouseInput.Farm.MousePosition.ReadValue<Vector2>();
        Camera mainCamera = GameObject.Find("MainCamera").GetComponent<Camera>();
        //int x = getExactPosition(mousePosition.x);
        //int y = getExactPosition(mousePosition.y);
        int x = getExactPosition(mousePosition.x);
        int y = mainCamera.pixelHeight - getExactPosition(mousePosition.y);
        Vector3 pos = mainCamera.ScreenToWorldPoint(new Vector3(x, y, mainCamera.nearClipPlane));
        //Vector3 pos = mainCamera.ScreenToWorldPoint(mousePosition);
        Debug.Log(pos.x + " " + pos.y);
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private int getExactPosition(double value)
    {
        int result = Convert.ToInt32(Math.Floor(value));
        return result;
    }
}
