﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using SocketIO;
using Cinemachine;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviour {

	public static NetworkManager instance;
	public Canvas canvas;
	public Canvas errorCanvas;
	public SocketIOComponent socket;
	public GameObject player;
	public GameObject otherPlayer;
	public ItemListJSON itemDetails;
	public ItemListJSON seeds;
	public ItemListJSON animals;
	public AnimalListJSON farmAnimals;
	public UserInfoJSON userInfo;
	public PlayerInventoryJSON playerInventory;
	[SerializeField] Text coinField;
	[SerializeField] GameObject CameraFollowPlayer;
	public PlantListJSON farmPlants;

	void Awake()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);
		//DontDestroyOnLoad(gameObject);
	}

	// Use this for initialization
	void Start () {
		// subscribe to all the various websocket events
		socket.Emit("handshake");

		this.JoinGame();

		socket.On("enemies", OnEnemies);
		socket.On("other player connected", OnOtherPlayerConnected);
		socket.On("play", OnPlay);
		socket.On("item response", OnAPIResponse);
		socket.On("userinventory response", OnUserInventoryResponse);
		socket.On("seed-list response", OnSeedListResponse);
		socket.On("farm-animal response", OnAnimalFarmResponse);
		socket.On("animal-list response", OnAnimalListResponse);
		socket.On("userinfo response", OnUserInfoResponse);
		socket.On("player move", OnPlayerMove);
		socket.On("player turn", OnPlayerTurn);
		socket.On("player shoot", OnPlayerShoot);
		socket.On("health", OnHealth);
		socket.On("other player disconnected", OnOtherPlayerDisconnect);
		socket.On("session timeout", OnSessionTimeout);
		socket.On("farm-plant response", OnFarmPlantResponse);

		this.CommandGetItemList(2);
		this.CommandGetItemList(3);
	}

    public void JoinGame()
	{
		StartCoroutine(ConnectToServer());
	}

	public void OnLeaveButtonClicked()
	{
		print("leave");
		SceneManager.LoadScene("Login");
	}

	#region Commands

	IEnumerator ConnectToServer()
	{
		print("token:" +PlayerPrefs.GetString("token"));
		if (PlayerPrefs.GetString("token") == "")
		{
			canvas.gameObject.SetActive(false);
			errorCanvas.gameObject.SetActive(true);
		}
		yield return new WaitForSeconds(0.5f);

		Scene scene = SceneManager.GetSceneAt(0);
		if (scene.name == "PersistentScene") scene = SceneManager.GetSceneAt(1);

		if (scene.name != "FarmShop2" && scene.name != "AccessoryStore" && scene.name != "FaceChanging" &&
			scene.name != "DressingRoom" && scene.name != "HairChanging")
        {
			SceneManager.SetActiveScene(SceneManager.GetSceneByName("PersistentScene"));
		}

		Debug.Log("scene" + scene.name);

		RoomJSON room = new RoomJSON(scene.name, PlayerPrefs.GetString("token"));
		string dataRoom = JsonUtility.ToJson(room);

		socket.Emit("player connect", new JSONObject(dataRoom));
		socket.On("session timeout", OnSessionTimeout);

		yield return new WaitForSeconds(1f);

		string playerName = PlayerPrefs.GetString("username");
        List<SpawnPoint> playerSpawnPoints = GetComponent<PlayerSpawner>().playerSpawnPoints;
        List<SpawnPoint> enemySpawnPoints = GetComponent<EnemySpawner>().enemySpawnPoints;
        PlayerJSON playerJSON = new PlayerJSON(playerName, playerSpawnPoints, enemySpawnPoints);
        string data = JsonUtility.ToJson(playerJSON);
        socket.Emit("play", new JSONObject(data));
//		if (scene.name != "FarmShop2") canvas.gameObject.SetActive(false);

		this.CommandAPI("user-info");
		this.CommandAPI("animal-list");
		this.CommandAPI("user-inventory");
		this.CommandAPI("plant-list");
		this.CommandGetItemList(1);
		this.CommandGetItemList(9);
		this.CommandGetItemList(2);
		this.CommandGetItemList(3);
	//	this.CommandAPI("user-inventory");
	}

	public void CommandDisconnect()
	{
		socket.Emit("disconnect");
		Destroy(GameObject.Find(PlayerPrefs.GetString("username") + "(Clone)"));
	}

	public void CommandHarvestAnimal(int animalID)
	{
		print("emit  Harvest animal" + animalID);
		AnimalIDJSON animal = new AnimalIDJSON(animalID);
		socket.Emit("animal-harvest", new JSONObject(JsonUtility.ToJson(animal)));
		this.CommandAPI("user-inventory");
	}

	public void CommandPlantSeed(int plotID, int seedID)
	{
		print("emit plant on " + plotID);
		PlantJSON plant = new PlantJSON(plotID, seedID);
		socket.Emit("plot-plant", new JSONObject(JsonUtility.ToJson(plant)));
	}

	public void CommandPlantHarvest(int plotID, int seedID)
	{
		print("emit harvest on plot" + plotID);
		PlantJSON plant = new PlantJSON(plotID, seedID);
		socket.Emit("harvest-plant", new JSONObject(JsonUtility.ToJson(plant)));
		this.CommandAPI("user-inventory");
	}

	public void CommandPlantWater(int plotID, int seedID)
	{
		print("emit water on " + plotID);
		PlantJSON plant = new PlantJSON(plotID, seedID);
		socket.Emit("water-plant", new JSONObject(JsonUtility.ToJson(plant)));
	}

	public void CommandRemovePlant(int plotID, int seedID)
	{
		print("emit remove on " + plotID);
		PlantJSON plant = new PlantJSON(plotID, seedID);
		socket.Emit("remove-plant", new JSONObject(JsonUtility.ToJson(plant)));
		this.CommandAPI("plant-list");
		this.CommandAPI("plant-list");
	}


	public void CommandMove(Vector2 inputVec, Vector3 vec3)
	{
		string data = JsonUtility.ToJson(new PositionJSON(vec3));
		
		float[] pos = new float[] {
				vec3.x,
				vec3.y,
				vec3.z
		};

		float[] rotation = new float[] {inputVec.x, inputVec.y, inputVec.magnitude};


		UserJSON user = new UserJSON(PlayerPrefs.GetString("username"), pos, rotation, 100);

		socket.Emit("player move", new JSONObject(JsonUtility.ToJson(user)));
	}

	public void CommandTurn(Quaternion quat)
	{
		string data = JsonUtility.ToJson(new RotationJSON(quat));
		socket.Emit("player turn", new JSONObject(data));
	}

	public void CommandShoot()
	{
		print("Shoot");
		socket.Emit("player shoot");
	}

	public void CommandHealthChange(GameObject playerFrom, GameObject playerTo, int healthChange, bool isEnemy)
	{
		print("health change cmd");
		HealthChangeJSON healthChangeJSON = new HealthChangeJSON(playerTo.name, healthChange, playerFrom.name, isEnemy);
		socket.Emit("health", new JSONObject(JsonUtility.ToJson(healthChangeJSON)));
	}

	public void CommandAPI(string endpoint)
	{
		print("emit API call" + endpoint);
		APIJSON api = new APIJSON(endpoint);
		socket.Emit("api get", new JSONObject(JsonUtility.ToJson(api)));
	}

	public void CommandFeedAnimal(int animalID)
	{
		GameObject currentInventoryItem = GameObject.Find("UI/MainGameUICanvas/UICanvasGroup/UIInventory/UIInventorySlot(Clone)" + PlayerPrefs.GetInt("itemNumber"));
		Debug.Log(currentInventoryItem.name);
		int currentQuantity = Int32.Parse(currentInventoryItem.transform.GetChild(1).GetComponent<Text>().text);
		Debug.Log("Quantity: " + currentQuantity);
		if (currentQuantity > 1)
		{
			currentInventoryItem.transform.GetChild(1).GetComponent<Text>().text = (currentQuantity - 1) + "";
		}
		else
		{
			Destroy(currentInventoryItem);
		}

		print("emit Feed animal" + animalID);
		AnimalIDJSON animal = new AnimalIDJSON(animalID);
		socket.Emit("animal-feed", new JSONObject(JsonUtility.ToJson(animal)));
	}

	public void CommandCollectAnimal(int animalID)
	{
		print("emit Collect animal" + animalID);
		AnimalIDJSON animal = new AnimalIDJSON(animalID);
		socket.Emit("animal-collect", new JSONObject(JsonUtility.ToJson(animal)));
	}

	public void CommandGetItemList(int itemType)
	{
		String endpoint = "item-list";
		print("emit API call item " + itemType);
		GetItemJSON api = new GetItemJSON(endpoint, itemType);
		socket.Emit("api get", new JSONObject(JsonUtility.ToJson(api)));
	}

	public void CommandPurchaseItem(int itemID)
	{
		print("emit PurchaseItem" +itemID);
		ItemPurchaseJSON item = new ItemPurchaseJSON(itemID);
		socket.Emit("purchase", new JSONObject(JsonUtility.ToJson(item)));
	}

	public void CommandSellInventory(int itemID, int quantity)
	{
		print("emit SellInventory" + itemID + ":" + quantity);
		SellInventoryJSON item = new SellInventoryJSON(itemID, quantity);
		socket.Emit("sell-inventory", new JSONObject(JsonUtility.ToJson(item)));
	}

	public void CommandEquipInventory(int itemID)
	{
		print("emit EquipInventory" + itemID);
		EquipInventoryJSON item = new EquipInventoryJSON(itemID);
		socket.Emit("equip-inventory", new JSONObject(JsonUtility.ToJson(item)));
	}

	#endregion

	#region Listening
	private void OnUserInventoryResponse(SocketIOEvent obj)
	{
		string data = obj.data.ToString();
		Debug.Log("userinventory: " + data);

		playerInventory = PlayerInventoryJSON.CreateFromJSON(data);
	}
	private void OnFarmPlantResponse(SocketIOEvent obj)
	{
		string data = obj.data.ToString();
		Debug.Log("Farm-plant: " + data);

		farmPlants = PlantListJSON.CreateFromJSON(data);
	}

	private void OnAnimalFarmResponse(SocketIOEvent obj)
	{
		string data = obj.data.ToString();
		Debug.Log("animalFarm: " + data);

		farmAnimals = AnimalListJSON.CreateFromJSON(data);
		List<FarmAnimalJSON> list = farmAnimals.data;
	}

	private void OnAPIResponse(SocketIOEvent obj)
	{

		string data = obj.data.ToString();

		itemDetails = ItemListJSON.CreateFromJSON(data);
		for (int i = 0; i < itemDetails.data.Count; i++)
		{
			Debug.Log(itemDetails.data[i].description);
		}
	}

	private void OnSeedListResponse(SocketIOEvent obj)
	{
		string data = obj.data.ToString();
		Debug.Log("seeds: " + data);

		seeds = ItemListJSON.CreateFromJSON(data);
	}

	private void OnAnimalListResponse(SocketIOEvent obj)
	{
		string data = obj.data.ToString();
		Debug.Log("animals: " + data);

		animals = ItemListJSON.CreateFromJSON(data);
	}

	private void OnUserInfoResponse(SocketIOEvent obj)
	{
		string data = obj.data.ToString();
		Debug.Log("userinfo: " + data);

		userInfo = PlayerDetailJSON.CreateFromJSON(data).data;

		coinField.text = userInfo.money.ToString();
	}


/*	private void OnUserPurchaseResponse(SocketIOEvent obj)
	{
		string data = obj.data.ToString();
		Debug.Log("user purchase: " + data);

		playerDetail = PlayerDetailJSON.CreateFromJSON(data);
	}*/

	void OnEnemies(SocketIOEvent socketIOEvent)
	{
		EnemiesJSON enemiesJSON = EnemiesJSON.CreateFromJSON(socketIOEvent.data.ToString());
		EnemySpawner es = GetComponent<EnemySpawner>();
		es.SpawnEnemies(enemiesJSON);
	}

	void OnSessionTimeout(SocketIOEvent socketIOEvent)
	{
		print("timeout emitted");
		//PlayerPrefs.DeleteKey("token");
		Destroy(this);
		canvas.gameObject.SetActive(false);
		errorCanvas.gameObject.SetActive(true);
		socket.Emit("disconnect");
	}

	void OnOtherPlayerConnected(SocketIOEvent socketIOEvent)
	{
		print("Someone else joined");
		string data = socketIOEvent.data.ToString();
		UserJSON userJSON = UserJSON.CreateFromJSON(data);
		Vector3 position = new Vector3(userJSON.position[0], userJSON.position[1], userJSON.position[2]);
		Quaternion rotation = Quaternion.Euler(userJSON.rotation[0], userJSON.rotation[1], userJSON.rotation[2]);
		GameObject o = GameObject.Find(userJSON.name) as GameObject;
		if (o != null)
		{
			return;
		}

		player.name = userJSON.name;

		GameObject p = Instantiate(otherPlayer, position, rotation) as GameObject;
		p.name = userJSON.name;
		Animator animator = p.GetComponent<Animator>();
		animator.SetFloat("Horizontal", userJSON.rotation[0]);
		animator.SetFloat("Vertical", userJSON.rotation[1]);
		animator.SetFloat("Speed", userJSON.rotation[2]);
		Debug.Log(p.name);
		// here we are setting up their other fields name and if they are local
		PlayerMovement pc = p.GetComponent<PlayerMovement>();
		Transform t = p.transform.Find("Healthbar Canvas");
		Transform t1 = t.transform.Find("Player Name");
		Text playerName = t1.GetComponent<Text>();
		playerName.text = userJSON.name;

		Debug.Log(userJSON.name);

		
		// we also need to set the health
		Health h = p.GetComponent<Health>();
		h.currentHealth = userJSON.health;
		h.OnChangeHealth();
	}
	void OnPlay(SocketIOEvent socketIOEvent)
	{
		print("you joined");
		string data = socketIOEvent.data.ToString();
		UserJSON currentUserJSON = UserJSON.CreateFromJSON(data);
		Vector3 position = new Vector3(currentUserJSON.position[0], currentUserJSON.position[1], currentUserJSON.position[2]);
		Quaternion rotation = Quaternion.Euler(currentUserJSON.rotation[0], currentUserJSON.rotation[1], currentUserJSON.rotation[2]);
		Debug.Log(currentUserJSON.name);

		player.name = currentUserJSON.name;

		GameObject well = GameObject.Find("Well");
		if (well != null) position = well.transform.position;

		GameObject p = Instantiate(player, position, rotation) as GameObject;
		print(p.name);
		p.transform.SetPositionAndRotation(position, rotation);
		CinemachineVirtualCamera vcam = CameraFollowPlayer.GetComponent<CinemachineVirtualCamera>();
		vcam.Follow = p.transform;
		Animator animator = p.GetComponent<Animator>();
		animator.SetFloat("Horizontal", currentUserJSON.rotation[0]);
		animator.SetFloat("Vertical", currentUserJSON.rotation[1]);
		animator.SetFloat("Speed", currentUserJSON.rotation[2]);

		Transform t = p.transform.Find("Healthbar Canvas");
		Transform t1 = t.transform.Find("Player Name");
		Text playerName = t1.GetComponent<Text>();
		playerName.text = currentUserJSON.name;

		PlayerMovement pc = GameObject.Find(currentUserJSON.name + "(Clone)").GetComponent<PlayerMovement>();


		p.name = currentUserJSON.name;

	}
	void OnPlayerMove(SocketIOEvent socketIOEvent)
	{
		string data = socketIOEvent.data.ToString();

		UserJSON userJSON = UserJSON.CreateFromJSON(data);
		Debug.Log(userJSON.name);

		Vector3 position = new Vector3(userJSON.position[0], userJSON.position[1], userJSON.position[2]);

		
		// if it is the current player exit
		if (userJSON.name == PlayerPrefs.GetString("username"))
		{
			return;
		}

		GameObject p = GameObject.Find(userJSON.name);

		if (p != null)
		{
			bool flipped = userJSON.rotation[0] < 0;
			if (flipped)
			{
				p.transform.localScale = new Vector3(1, 1, 1);
			}
			else
			{
				p.transform.localScale = new Vector3(-1, 1, 1);
			}

			p.transform.position = position;
			Animator animator = p.GetComponent<Animator>();
			animator.SetFloat("Horizontal", userJSON.rotation[0]);
			animator.SetFloat("Vertical", userJSON.rotation[1]);
			animator.SetFloat("Speed", userJSON.rotation[2]);
		}
	}
	void OnPlayerTurn(SocketIOEvent socketIOEvent)
	{
		string data = socketIOEvent.data.ToString();
		UserJSON userJSON = UserJSON.CreateFromJSON(data);
		Quaternion rotation = Quaternion.Euler(userJSON.rotation[0], userJSON.rotation[1], userJSON.rotation[2]);
		// if it is the current player exit
		if (userJSON.name == PlayerPrefs.GetString("username"))
		{
			return;
		}
		GameObject p = GameObject.Find(userJSON.name) as GameObject;
		if (p != null)
		{
			p.transform.rotation = rotation;
		}
	}
	void OnPlayerShoot(SocketIOEvent socketIOEvent)
	{
		string data = socketIOEvent.data.ToString();
		ShootJSON shootJSON = ShootJSON.CreateFromJSON(data);
		//find the gameobject
		GameObject p = GameObject.Find(shootJSON.name);
		// instantiate the bullet etc from the player script
		PlayerController pc = p.GetComponent<PlayerController>();
		pc.CmdFire();
	}

	[Serializable]
	public class PlayerInventoryJSON
	{
		public bool success;

		public List<UserInventoryJSON> data;

		public PlayerInventoryJSON(bool success, List<UserInventoryJSON> data)
		{
			this.success = success;
			this.data = data;
		}

		public static PlayerInventoryJSON CreateFromJSON(string data)
		{
			return JsonUtility.FromJson<PlayerInventoryJSON>(data);
		}
	}


	[Serializable]
	public class UserInventoryJSON
	{
		public int id;
		public int user_id;
		public int item_id;
		public int item_type;
		public int quantity;
		public ItemJSON item;

		public UserInventoryJSON(int id, int user_id, int item_id, int item_type, int quantity, ItemJSON item)
		{
			this.id = id;
			this.user_id = user_id;
			this.item_id = item_id;
			this.item_type = item_type;
			this.quantity = quantity;
			this.item = item;
		}
	}

	[Serializable]
	public class RoomJSON
	{
		public string room;
		public string token;

		public RoomJSON(String _room, String _token)
		{
			room = _room;
			token = _token;
		}
	}

	void OnHealth(SocketIOEvent socketIOEvent)
	{
		print("changing the health");
		// get the name of the player whose health changed
		string data = socketIOEvent.data.ToString();
		UserHealthJSON userHealthJSON = UserHealthJSON.CreateFromJSON(data);
		GameObject p = GameObject.Find(userHealthJSON.name);
		Health h = p.GetComponent<Health>();
		h.currentHealth = userHealthJSON.health;
		h.OnChangeHealth();
	}

	void OnOtherPlayerDisconnect(SocketIOEvent socketIOEvent)
	{
		print("user disconnected");
		string data = socketIOEvent.data.ToString();
		UserJSON userJSON = UserJSON.CreateFromJSON(data);

		Destroy(GameObject.Find(userJSON.name));
	}

	#endregion

	#region JSONMessageClasses
	[Serializable]
	public class PlantJSON
	{
		public int plot_id;
		public int seed_id;

		public PlantJSON(int _plot_id, int _seed_id)
		{
			plot_id = _plot_id;
			seed_id = _seed_id;
		}
	}

	[Serializable]
	public class PlantListJSON
	{
		public bool success;
		public List<FarmPlantJSON> data;

		public PlantListJSON(List<FarmPlantJSON> _data)
		{
			success = true;
			data = _data;
		}

		public static PlantListJSON CreateFromJSON(string data)
		{
			return JsonUtility.FromJson<PlantListJSON>(data);
		}
	}

	[Serializable]
	public class FarmPlantJSON
	{
		public int id;
		public int user_id;
		public int plot_id;
		public int item_id;
		public int product_id;
		public string start_time;
		public int growing_time;
		public string sprite_link;
		public string last_fed_time;
		public int dying_time;
		public int status;
		public ItemJSON item;
		public ItemJSON product;

        public FarmPlantJSON(int id, int user_id, int plot_id, int item_id, int product_id, string start_time, int growing_time, string sprite_link, string last_fed_time, int dying_time, int status, ItemJSON item, ItemJSON product)
        {
            this.id = id;
            this.user_id = user_id;
            this.plot_id = plot_id;
            this.item_id = item_id;
            this.product_id = product_id;
            this.start_time = start_time;
            this.growing_time = growing_time;
            this.sprite_link = sprite_link;
            this.last_fed_time = last_fed_time;
            this.dying_time = dying_time;
            this.status = status;
            this.item = item;
            this.product = product;
        }
    }

	[Serializable]
	public class PlayerJSON
	{
		public string name;
		public List<PointJSON> playerSpawnPoints;
		public List<PointJSON> enemySpawnPoints;

		public PlayerJSON(string _name, List<SpawnPoint> _playerSpawnPoints, List<SpawnPoint> _enemySpawnPoints)
		{
			playerSpawnPoints = new List<PointJSON>();
			enemySpawnPoints = new List<PointJSON>();
			name = _name;
			foreach (SpawnPoint playerSpawnPoint in _playerSpawnPoints)
			{
				PointJSON pointJSON = new PointJSON(playerSpawnPoint);
				playerSpawnPoints.Add(pointJSON);
			}
			foreach (SpawnPoint enemySpawnPoint in _enemySpawnPoints)
			{
				PointJSON pointJSON = new PointJSON(enemySpawnPoint);
				enemySpawnPoints.Add(pointJSON);
			}
		}
	}

	[Serializable]
	public class PointJSON
	{
		public float[] position;
		public float[] rotation;
		public PointJSON(SpawnPoint spawnPoint)
		{
			position = new float[] {
				spawnPoint.transform.position.x,
				spawnPoint.transform.position.y,
				spawnPoint.transform.position.z
			};
			rotation = new float[] {
				spawnPoint.transform.eulerAngles.x,
				spawnPoint.transform.eulerAngles.y,
				spawnPoint.transform.eulerAngles.z
			};
		}
	}

	[Serializable]
	public class APIJSON
	{
		public string endpoint;


		public APIJSON(String _endpoint)
		{
			endpoint = _endpoint;
		}
	}


	[Serializable]
	public class AnimalIDJSON
	{
		public int animal_id;


		public AnimalIDJSON(int _animal_id)
		{
			animal_id = _animal_id;
		}
	}

	[Serializable]
	public class GetItemJSON
	{
		public string endpoint;
		public int item_type;


		public GetItemJSON(String _endpoint, int _itemType)
		{
			endpoint = _endpoint;
			item_type = _itemType;
		}
	}

	[Serializable]
	public class ItemPurchaseJSON
	{
		public int itemID;

		public ItemPurchaseJSON(int _itemID)
		{
			itemID = _itemID;
		}
	}

	[Serializable]
	public class SellInventoryJSON
	{
		public int item_id;
		public int quantity;

		public SellInventoryJSON(int _itemID, int _quantity)
		{
			item_id = _itemID;
			quantity = _quantity;
		}
	}

	[Serializable]
	public class EquipInventoryJSON
	{
		public int item_id;

		public EquipInventoryJSON(int _itemID)
		{
			item_id = _itemID;
		}
	}


	[Serializable]
	public class PlayerDetailJSON
	{
		public bool success;
		public UserInfoJSON data;

        public PlayerDetailJSON(bool success,UserInfoJSON data)
        {
            this.success = success;
			this.data = data;
        }

        public static PlayerDetailJSON CreateFromJSON(string data)
		{
			return JsonUtility.FromJson<PlayerDetailJSON>(data);
		}
	}


	[Serializable]
	public class LoginInfoJSON
	{
		public int status_code;
		public string access_token;
		public string token_type;
		public UserAccountJSON user_info;

        public LoginInfoJSON(int status_code, string access_token, string token_type, UserAccountJSON user_info)
        {
            this.status_code = status_code;
            this.access_token = access_token;
            this.token_type = token_type;
			this.user_info = user_info;
        }

        public static LoginInfoJSON CreateFromJSON(string data)
		{
			return JsonUtility.FromJson<LoginInfoJSON>(data);
		}
	}

	[Serializable]
	public class UserInfoJSON
	{
		public int id;
		public int user_id;
		public int money;
		public int plot_own;
		public int suit_id;
		public int hair_id;
		public int hat_id;
		public int face_id;
		public int face_decor_id;
		public ItemJSON hair;
		public ItemJSON hat;
		public ItemJSON face;
		public ItemJSON face_decor;
		public ItemJSON suit;

		public UserInfoJSON(int id, int user_id, int money, int plot_own, int suit_id, int hair_id, int hat_id, int face_id, int face_decor_id, ItemJSON hair, ItemJSON hat, ItemJSON face, ItemJSON face_decor, ItemJSON suit)
		{
			this.id = id;
			this.user_id = user_id;
			this.money = money;
			this.plot_own = plot_own;
			this.suit_id = suit_id;
			this.hair_id = hair_id;
			this.hat_id = hat_id;
			this.face_id = face_id;
			this.face_decor_id = face_decor_id;
			this.hair = hair;
			this.hat = hat;
			this.face = face;
			this.face_decor = face_decor;
			this.suit = suit;
		}
	}

	[Serializable]
	public class UserAccountJSON
	{
		public int id;
		public string name;
		public string username;
		public string email;
		public string created_at;
		public string updated_at;
		public int status;

        public UserAccountJSON(int id, string name, string username, string email, string created_at, string updated_at, int status)
        {
            this.id = id;
            this.name = name;
            this.username = username;
            this.email = email;
            this.created_at = created_at;
            this.updated_at = updated_at;
            this.status = status;
        }
    }

	[Serializable]
	public class ItemListJSON
	{
		public bool success;
		public List<ItemJSON> data;

		public ItemListJSON(List<ItemJSON> _itemDetails)
		{
			success = true;
			data = _itemDetails;
		}

		public static ItemListJSON CreateFromJSON(string data)
		{
			return JsonUtility.FromJson<ItemListJSON>(data);
		}
	}

	[Serializable]
	public class AnimalListJSON
	{
		public bool success;
		public List<FarmAnimalJSON> data;

		public AnimalListJSON(List<FarmAnimalJSON> _farmAnimals)
		{
			success = true;
			data = _farmAnimals;
		}

		public static AnimalListJSON CreateFromJSON(string data)
		{
			return JsonUtility.FromJson<AnimalListJSON>(data);
		}
	}

	[Serializable]
	public class FarmAnimalJSON
	{
		public int id;
		public int user_id;
		public int item_id;
		public int product_id;
		public string start_time;
		public int growing_time;
		public string prefab_link;
		public string last_fed_time;
		public int dying_time;
		public int collectable_count;
		public string last_collect_time;
		public string next_collect_time;
		public int status;
		public ItemJSON item;
		public ItemJSON product;
	}

	[Serializable]
	public class ItemJSON
	{
		public int id;
		public string name;
		public int price;
		public int item_type;
		public string sprite;
		public string description;
		public string prefab_link;
		public string product_id;
		public int time_to_grow;
		public int time_to_live;
		public int colletable_count;
		public int time_to_collect;
		public string grow_prefab_link;

        public ItemJSON(int id, string name, int price, int item_type, string sprite, string description, string prefab_link, string product_id, int time_to_grow, int time_to_live, int colletable_count, int time_to_collect, string grow_prefab_link)
        {
            this.id = id;
            this.name = name;
            this.price = price;
            this.item_type = item_type;
            this.sprite = sprite;
            this.description = description;
            this.prefab_link = prefab_link;
            this.product_id = product_id;
            this.time_to_grow = time_to_grow;
            this.time_to_live = time_to_live;
            this.colletable_count = colletable_count;
            this.time_to_collect = time_to_collect;
            this.grow_prefab_link = grow_prefab_link;
        }
    }

	[Serializable]
	public class PositionJSON
	{
		public float[] position;

		public PositionJSON(Vector3 _position)
		{
			position = new float[] { _position.x, _position.y, _position.z };
		}
	}

	[Serializable]
	public class RotationJSON
	{
		public float[] rotation;

		public RotationJSON(Quaternion _rotation)
		{
			rotation = new float[] { _rotation.eulerAngles.x,
				_rotation.eulerAngles.y, 
				_rotation.eulerAngles.z };
		}
	}

	[Serializable]
	public class UserJSON
	{
		public string name;
		public float[] position;
		public float[] rotation;
		public int health;


		public UserJSON(string _name, float[] _position, float[] _rotation, int _health)
		{
			name = _name;
			rotation = _rotation;
			position = _position;
			health = _health;
		}

		public static UserJSON CreateFromJSON(string data)
		{
			return JsonUtility.FromJson<UserJSON>(data);
		}
	}

	[Serializable]
	public class HealthChangeJSON
	{
		public string name;
		public int healthChange;
		public string from;
		public bool isEnemy;

		public HealthChangeJSON(string _name, int _healthChange, string _from, bool _isEnemy)
		{
			name = _name;
			healthChange = _healthChange;
			from = _from;
			isEnemy = _isEnemy;
		}
	}

	[Serializable]
	public class EnemiesJSON
	{
		public List<UserJSON> enemies;

		public static EnemiesJSON CreateFromJSON(string data)
		{
			return JsonUtility.FromJson<EnemiesJSON>(data);
		}
	}

	[Serializable]
	public class ShootJSON
	{
		public string name;

		public static ShootJSON CreateFromJSON(string data)
		{
			return JsonUtility.FromJson<ShootJSON>(data);
		}
	}

	[Serializable]
	public class UserHealthJSON
	{
		public string name;
		public int health;

		public static UserHealthJSON CreateFromJSON(string data)
		{
			return JsonUtility.FromJson<UserHealthJSON>(data);
		}
	}

	[Serializable]
	public class ItemInventoryListJSON
	{
		public bool success;
		public List<ItemInventoryJSON> data;

		public ItemInventoryListJSON(List<ItemInventoryJSON> _itemDetails)
		{
			success = true;
			data = _itemDetails;
		}

		public static ItemListJSON CreateFromJSON(string data)
		{
			return JsonUtility.FromJson<ItemListJSON>(data);
		}
	}

	[Serializable]
	public class ItemInventoryJSON
	{
		public int id;
		public int user_id;
		public int item_id;
		public int item_type;
		public int quantity;

        public ItemInventoryJSON(int id, int user_id, int item_id, int item_type, int quantity)
        {
            this.id = id;
            this.user_id = user_id;
            this.item_id = item_id;
            this.item_type = item_type;
            this.quantity = quantity;
        }
    }


	#endregion
}
