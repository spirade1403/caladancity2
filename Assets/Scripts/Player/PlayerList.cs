using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "playerList", menuName = "Scriptable Objects/Player/Player List")]

public class PlayerList : ScriptableObject
{
    [SerializeField]
    public List<PlayerDetail> playerDetails;

    public int ItemCount
    {
        get
        {
            return playerDetails.Count;
        }
    }

    public PlayerDetail GetItem(int index)
    {
        return playerDetails[index];
    }

    public PlayerDetail GetUserByID (int id)
    {
        for (int i = 0; i < playerDetails.Count; i++)
        {
            if (playerDetails[i].userID == id)
            {
                return playerDetails[i];
            }
        }
        return null;
    }


}
