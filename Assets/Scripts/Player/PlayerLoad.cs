using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static NetworkManager;

public class PlayerLoad : MonoBehaviour
{
    GameObject networkManager;
    public UserInfoJSON userInfo;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
    }

    void LateUpdate()
    {
        networkManager = GameObject.Find("Network Manager");
        NetworkManager n = networkManager.GetComponent<NetworkManager>();
        userInfo = n.userInfo;

        GameObject.Find(PlayerPrefs.GetString("name") + "(Clone)/body/head/face").transform.GetComponent<SpriteRenderer>().sprite
           = Resources.Load<Sprite>(userInfo.face.sprite);
        GameObject.Find(PlayerPrefs.GetString("name") + "(Clone)/body/head/face/face-decoration").transform.GetComponent<SpriteRenderer>().sprite
            = Resources.Load<Sprite>(userInfo.face_decor.sprite);
        GameObject.Find(PlayerPrefs.GetString("name") + "(Clone)/body/head/hair").transform.GetComponent<SpriteRenderer>().sprite
            = Resources.Load<Sprite>(userInfo.hair.sprite);
        GameObject.Find(PlayerPrefs.GetString("name") + "(Clone)/body/head/hat").transform.GetComponent<SpriteRenderer>().sprite
            = Resources.Load<Sprite>(userInfo.hat.sprite);
        GameObject.Find(PlayerPrefs.GetString("name") + "(Clone)/body").transform.GetComponent<SpriteRenderer>().sprite
           = Resources.Load<Sprite>(userInfo.suit.sprite + "/body-idle");
        GameObject.Find(PlayerPrefs.GetString("name") + "(Clone)/body/back-arm").transform.GetComponent<SpriteRenderer>().sprite
           = Resources.Load<Sprite>(userInfo.suit.sprite + "/back-arm1");
        GameObject.Find(PlayerPrefs.GetString("name") + "(Clone)/body/front-arm").transform.GetComponent<SpriteRenderer>().sprite
           = Resources.Load<Sprite>(userInfo.suit.sprite + "/front-arm1");
        GameObject.Find(PlayerPrefs.GetString("name") + "(Clone)/ID").transform.GetComponent<Text>().text
          = PlayerPrefs.GetString("id");
        GameObject.Find(PlayerPrefs.GetString("name") + "(Clone)/ID").transform.GetComponent<Text>().transform.localScale = Vector3.zero;
    }
}
