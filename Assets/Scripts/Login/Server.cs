﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using static NetworkManager;

public class Server : MonoBehaviour
{
	[SerializeField] GameObject welcomePanel;
	[SerializeField] GameObject loginPanel;
	[Space]
	[SerializeField] InputField username;
	[SerializeField] InputField password;

	[SerializeField] Text errorMessages;


	[SerializeField] Button loginButton;

	
	[SerializeField] string url;

	WWWForm form;

	public void OnLoginButtonClicked ()
	{
		print("attempted login");
		loginButton.interactable = false;
		StartCoroutine(Login());
	}

	public void OnCloseAchivementButtonClicked()
	{
		SceneManager.LoadScene("scene3_WorldMap");
	}

	[System.Obsolete]
    IEnumerator Login ()
	{
		form = new WWWForm ();

		form.AddField ("username", username.text);
		form.AddField ("password", password.text);

		WWW w = new WWW(url, form);
		yield return w;
		Debug.Log(w.text);
		if (w.error != null) {
			errorMessages.text = "Error! Cannot connect to Server !!!";
			Debug.Log("<color=red>"+w.text+"</color>");//error
		} else {
			if (w.isDone) {
				if (w.text.Contains ("error")) {
					errorMessages.text = "Invalid username or password!";
					Debug.Log("<color=red>"+w.text+"</color>");//error
				} else {
					//open welcom panel
					loginPanel.SetActive(false);
					welcomePanel.SetActive (true);
					Debug.Log("<color=green>"+w.text+"</color>");//user exist
					LoginInfoJSON info = LoginInfoJSON.CreateFromJSON(w.text);
					PlayerPrefs.SetString("token", info.access_token);
					PlayerPrefs.SetString("username", info.user_info.username);
					PlayerPrefs.SetString("name", info.user_info.name);
					PlayerPrefs.SetString("id", info.user_info.id.ToString());
					PlayerPrefs.Save();
					print(info.access_token);
					print(info.user_info);
				}
			}
		}

		loginButton.interactable = true;

		w.Dispose ();
	} 

	public void GoToRegisterPage()
    {
		Application.OpenURL("http://103.57.222.114:8081/#/register");
	}
}
