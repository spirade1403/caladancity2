using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static NetworkManager;

public class HairSalon : MonoBehaviour
{
    GameObject gameObject;
    [SerializeField] GameObject networkManager;
    [SerializeField] GameObject itemTemplate;
    [SerializeField] Transform shopScrollView;
    [SerializeField] public SpriteRenderer bodyPart;

    [SerializeField] Text coinField;
    public int bodyPartStr;
    public GameObject errorCanvas;

    
    // Start is called before the first frame update
    IEnumerator Start()
    {
        string dummy = PlayerPrefs.GetString("name");
        NetworkManager n = networkManager.GetComponent<NetworkManager>();

        yield return new WaitForSeconds(2.5f);
        List<ItemJSON> itemList= n.itemDetails.data;
        UserInfoJSON userInfo = n.userInfo;
        List<UserInventoryJSON> inventoryList = n.playerInventory.data;
        GameObject player = GameObject.Find(PlayerPrefs.GetString("name"));

        if (player == null) dummy = "Player";

        GameObject.Find(dummy + "/body/head/face").transform.GetComponent<SpriteRenderer>().sprite
         = Resources.Load<Sprite>(userInfo.face.sprite);
        GameObject.Find(dummy + "/body/head/face/face-decoration").transform.GetComponent<SpriteRenderer>().sprite
            = Resources.Load<Sprite>(userInfo.face_decor.sprite);
        GameObject.Find(dummy + "/body/head/hair").transform.GetComponent<SpriteRenderer>().sprite
            = Resources.Load<Sprite>(userInfo.hair.sprite);
        GameObject.Find(dummy + "/body/head/hat").transform.GetComponent<SpriteRenderer>().sprite
            = Resources.Load<Sprite>(userInfo.hat.sprite);

        GameObject.Find(dummy + "/body").transform.GetComponent<SpriteRenderer>().sprite
           = Resources.Load<Sprite>(userInfo.suit.sprite + "/body-idle");
        GameObject.Find(dummy + "/body/front-arm").transform.GetComponent<SpriteRenderer>().sprite
           = Resources.Load<Sprite>(userInfo.suit.sprite + "/front-arm1");
        GameObject.Find(dummy + "/body/back-arm").transform.GetComponent<SpriteRenderer>().sprite
           = Resources.Load<Sprite>(userInfo.suit.sprite + "/back-arm1");


        int len = itemList.Count;
        coinField.text = userInfo.money.ToString();

        for (int i = 0; i < len; i++)
        {
            if (itemList[i].item_type != bodyPartStr) continue;

            gameObject = Instantiate(itemTemplate, shopScrollView);

            //Object icon
            if (itemList[i].item_type == 8)
            {
                gameObject.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(itemList[i].sprite + "/body-idle");
            }
            else
            {
                gameObject.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(itemList[i].sprite);
            }

            //Object Price
            gameObject.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = itemList[i].price.ToString();

            //Button Change
            Button btnChange = gameObject.transform.GetChild(2).GetComponent<Button>();
            string bodySprite = itemList[i].sprite;

            btnChange.onClick.AddListener(delegate()
            {
                Debug.Log("thay quan ao " + bodySprite);
              
                if(bodyPartStr == 8)
                {
                    bodyPart.sprite = Resources.Load<Sprite>(bodySprite + "/body-idle");
                    GameObject.Find(dummy + "/body/back-arm").transform.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(bodySprite + "/back-arm1");
                    GameObject.Find(dummy + "/body/front-arm").transform.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(bodySprite + "/front-arm1");
                }
                else
                {
                    bodyPart.sprite = Resources.Load<Sprite>(bodySprite);
                }
                
            });

            //Button Buy
            Button btnBuy = gameObject.transform.GetChild(3).GetComponent<Button>();
            btnBuy.GetComponentInChildren<Text>().text = "BUY";
            btnBuy.enabled = true;

            ItemJSON itemDetail = itemList[i];

            UserInventoryJSON itemExist = GetItemByItemID(inventoryList, itemDetail.id);

            if (itemExist != null)
            {
                Debug.Log("Item exist:-------- " + itemExist.item_id);
                btnBuy.GetComponentInChildren<Text>().text = "BOUGHT";
                btnBuy.enabled = false;
            }
            else
            {
                btnBuy.GetComponentInChildren<Text>().text = "BUY";
                btnBuy.enabled = true;
            }
        
           
            int price = itemDetail.price;

            btnBuy.onClick.AddListener(delegate ()
            {
                int coin = userInfo.money;
                // buyBtn.transform.GetChild(0).GetComponent<Text>().text = "Onclick " + i;
                if (coin >= price)
                {
                    coin -= price;
                    coinField.text = coin.ToString();

                    n.CommandPurchaseItem(itemDetail.id);

                    if (bodyPartStr == 8)
                    {
                        GameObject.Find(dummy + "/body/back-arm").transform.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(itemDetail.sprite + "/back-arm1");
                        GameObject.Find(dummy + "/body/front-arm").transform.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(itemDetail.sprite + "/front-arm1");
                    }
                    
                    btnBuy.GetComponentInChildren<Text>().text = "BOUGHT";
                    btnBuy.enabled = false;
                }
                else
                {
                    errorCanvas.SetActive(true);
                    errorCanvas.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Text>().text = "Sorry, you don't have enough money";
                }


            });
        }
        //  Destroy(itemTemplate);

    }

    public UserInventoryJSON GetItemByItemID(List<UserInventoryJSON> itemList, int itemID)
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            if (itemList[i].item.id == itemID) return itemList[i];
        }

        return null;
    }
}
