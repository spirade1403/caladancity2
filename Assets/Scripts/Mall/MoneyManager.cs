using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyManager : MonoBehaviour
{
    [SerializeField] public PlayerList playerList;
   
    
    public void UseCoin(int amount)
    {
        playerList.GetItem(0).coin -= amount;
    }

    public bool HasEnoughCoin(int amount)
    {
        return (playerList.GetItem(0).coin >= amount);
    }
}
