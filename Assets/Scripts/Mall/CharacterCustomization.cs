using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CharacterCustomization : MonoBehaviour
{
    [Header("Srite to change")]
    public SpriteRenderer bodyPart;
    [Header("Srite to Cycle Through")]
    public CharacterDB characterDB;
    private int currentOption = (int)0;
    public Text price;
    public Text money;

    void Start()
    {
        price.text = characterDB.GetCharacter(currentOption).price.ToString();
        if (!PlayerPrefs.HasKey("currentOption"))
        {
            currentOption = 0;
        }
        else
        {
            Load();
        }
        UpdateCharacter(currentOption);
    }

    public void NextOption()
    {
        currentOption++;
        price.text = characterDB.GetCharacter(currentOption).price.ToString();

        if (currentOption > characterDB.CharacterCount - 1)
        {
            currentOption = 0;
        }
        UpdateCharacter(currentOption);
        // Save();
    }

    public void PreviousOption()
    {
        currentOption--;
        price.text = characterDB.GetCharacter(currentOption).price.ToString();
        if (currentOption < 0)
        {
            currentOption = characterDB.CharacterCount - 1;
        }
        UpdateCharacter(currentOption);
        // Save();
    }

    private void UpdateCharacter(int currentOption)
    {
        Character character = characterDB.GetCharacter(currentOption);
        bodyPart.sprite = character.characterSprite;
    }

    public void Load()
    {
        currentOption = PlayerPrefs.GetInt("currentOption");
    }

    public void Save()
    {
        PlayerPrefs.SetInt("currentOption", currentOption);
    }

    public void Buy()
    {
        Save();
        int firstMoney = int.Parse(money.text);
        money.text = (firstMoney - characterDB.GetCharacter(currentOption).price).ToString();
    }

}
