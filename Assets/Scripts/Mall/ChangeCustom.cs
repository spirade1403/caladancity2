using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeCustom : MonoBehaviour
{
    // Start is called before the first frame update
    [Header("Srite to change")]
    public SpriteRenderer bodyPart;
    [Header("Srite to Cycle Through")]
    public Sprite spriteTochange;
    public int currentOption;
    public Text money;
    public Text price;
    public CharacterDB characterDB;

    private void Start()
    {
        price.text = characterDB.GetCharacter(currentOption).price.ToString();
    }
    public void changeCustoms()
    {
        PlayerPrefs.SetInt("currentOption", currentOption);
        bodyPart.sprite = spriteTochange;
        int firstMoney = int.Parse(money.text);
        money.text = (firstMoney - characterDB.GetCharacter(currentOption).price).ToString();
    }

    public void playGame()
    {

        SceneManager.LoadScene("HairSaloon2");
    }
}
