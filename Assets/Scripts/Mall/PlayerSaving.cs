using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSaving : MonoBehaviour
{
    [Header("Srite to change")]
    public SpriteRenderer bodyPart;

    [Header("Srite to Cycle Through")]
    public CharacterDB characterDB;

    private int currentOption = (int)0;

    void Start()
    {
        if (!PlayerPrefs.HasKey("currentOption"))
        {
            currentOption = 0;
        }
        else
        {
            Load();
        }
        UpdateCharacter(currentOption);
    }

    private void UpdateCharacter(int currentOption)
    {
        Character character = characterDB.GetCharacter(currentOption);
        bodyPart.sprite = character.characterSprite;
    }

    public void Load()
    {
        currentOption = PlayerPrefs.GetInt("currentOption");
    }
}
