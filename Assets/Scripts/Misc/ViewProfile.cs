using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ViewProfile : MonoBehaviour
{
    // Start is called before the first frame update
    public PlayerList playerList;
    Canvas profileCanvas;
   [SerializeField] public PlayerList player1FriendRequestList;
    [SerializeField] public PlayerList currentPlayerFriendList;

    void Start()
    {
        profileCanvas = GameObject.Find("Other Profile").transform.GetChild(0).GetComponent<Canvas>();
        profileCanvas.GetComponent<RectTransform>().transform.localScale = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            //Debug.Log("Helloooooooo" + mousePos2D + "whose name is "+hit.collider.gameObject.name);
            if (hit.collider != null && hit.collider.gameObject.tag == "Player")
            {
                int id = Int32.Parse(hit.collider.gameObject.transform.GetChild(1).GetComponent<Text>().text);

                //check if this is current player
                if (id != 0)
                {
                    Debug.Log("This is: " + hit.collider.gameObject.name);
                    PlayerDetail playerDetail = playerList.GetUserByID(id);
                    profileCanvas.GetComponent<RectTransform>().transform.localScale = Vector3.one;

                    //Player Name
                    profileCanvas.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = playerDetail.username;
                    //username.text = playerDetail.username;

                    //Friendly stat
                    profileCanvas.transform.GetChild(1).GetChild(1).GetComponent<Slider>().maxValue = 100;
                    profileCanvas.transform.GetChild(1).GetChild(1).GetComponent<Slider>().value = playerDetail.friendlyStat;
                    profileCanvas.transform.GetChild(1).GetChild(1).GetComponent<Slider>().interactable = false;

                    //Money
                    profileCanvas.transform.GetChild(1).GetChild(0).GetChild(0).GetChild(0).transform.GetComponent<Text>().text = playerDetail.coin.ToString();
                    profileCanvas.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).transform.GetComponent<Text>().text = playerDetail.gem.ToString();
                    profileCanvas.transform.GetChild(1).GetChild(0).GetChild(2).GetChild(1).transform.GetComponent<Text>().text = playerDetail.level.ToString();

                    //Player outfit
                    if (playerDetail.faceSprite != "")
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).transform.GetComponent<Image>().sprite
                       = Resources.Load<Sprite>(playerDetail.faceSprite);
                    else
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).transform.GetComponent<Image>().enabled = false;

                    if (playerDetail.faceDecorationSprite != "")
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).transform.GetComponent<Image>().sprite
                        = Resources.Load<Sprite>(playerDetail.faceDecorationSprite);
                    else
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).transform.GetComponent<Image>().enabled = false;

                    if (playerDetail.hairSprite != "")
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(4).transform.GetComponent<Image>().sprite
                        = Resources.Load<Sprite>(playerDetail.hairSprite);
                    else
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(4).transform.GetComponent<Image>().enabled = false;

                    if (playerDetail.hatSprite != "")
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(5).transform.GetComponent<Image>().sprite
                        = Resources.Load<Sprite>(playerDetail.hatSprite);
                    else
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(5).transform.GetComponent<Image>().enabled = false;

                    if (playerDetail.bodySprite != "")
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(1).transform.GetComponent<Image>().sprite
                       = Resources.Load<Sprite>(playerDetail.bodySprite);
                    else
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(1).transform.GetComponent<Image>().enabled = false;

                    if (playerDetail.frontArmSprite != "")
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(3).transform.GetComponent<Image>().sprite
                       = Resources.Load<Sprite>(playerDetail.frontArmSprite);
                    else
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(3).transform.GetComponent<Image>().enabled = false;

                    if (playerDetail.backHairSprite != "")
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).transform.GetComponent<Image>().sprite
                     = Resources.Load<Sprite>(playerDetail.backHairSprite);
                    else
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).transform.GetComponent<Image>().enabled = false;

                    if (playerDetail.backArmSprite != "")
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).transform.GetComponent<Image>().sprite
                    = Resources.Load<Sprite>(playerDetail.backArmSprite);
                    else
                        profileCanvas.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).transform.GetComponent<Image>().enabled = false;


                    //Buton Add Friend
                    if (player1FriendRequestList.GetUserByID(0) != null)
                    {
                        profileCanvas.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = "Sent friend request";
                    }else if(currentPlayerFriendList.GetUserByID(id) != null)
                    {
                        profileCanvas.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = "Friend";
                    }
                    else
                    {
                        profileCanvas.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = "Add friend";
                    }
                    profileCanvas.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(delegate ()
                    {
                        profileCanvas.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = "Sent friend request";
                        player1FriendRequestList.playerDetails.Add(playerList.GetItem(0));
                    });
                }
            }
        }
    }
}
