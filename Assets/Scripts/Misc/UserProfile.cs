 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserProfile : MonoBehaviour
{
    public PlayerList playerList;
    public GameObject mainCanvas;
    public Slider friendlyStat;
   // public Text username;
    // Start is called before the first frame update
    void Start()
    {
        mainCanvas.SetActive(false);

       
    }

    public void DisplayCanvas()
    {
        mainCanvas.SetActive(true);

        //Player Name
        GameObject.Find("Profile/PlayerHolder/PlayerName").transform.GetComponent<Text>().text = playerList.GetItem(0).username;
        //username.text = playerList.GetItem(0).username;

        //Friendly stat
        friendlyStat.maxValue = 100;
        friendlyStat.value = playerList.GetItem(0).friendlyStat;
        friendlyStat.interactable = false;

        //Money
        GameObject.Find("Profile/PlayerInfor/Information/Coin/Text").transform.GetComponent<Text>().text = playerList.GetItem(0).coin.ToString();
        GameObject.Find("Profile/PlayerInfor/Information/Diamond/Text").transform.GetComponent<Text>().text = playerList.GetItem(0).gem.ToString();
        GameObject.Find("Profile/PlayerInfor/Information/Level/LevelNumber").transform.GetComponent<Text>().text = playerList.GetItem(0).level.ToString();

        //Player outfit
        if (playerList.GetItem(0).faceSprite != "")
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Head/Face").transform.GetComponent<Image>().sprite
           = Resources.Load<Sprite>(playerList.GetItem(0).faceSprite);
        else
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Head/Face").transform.GetComponent<Image>().enabled = false;

        if (playerList.GetItem(0).faceDecorationSprite != "")
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Head/Face/FaceDecoration").transform.GetComponent<Image>().sprite
            = Resources.Load<Sprite>(playerList.GetItem(0).faceDecorationSprite);
        else
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Head/Face/FaceDecoration").transform.GetComponent<Image>().enabled = false;

        if (playerList.GetItem(0).hairSprite != "")
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Hair").transform.GetComponent<Image>().sprite
            = Resources.Load<Sprite>(playerList.GetItem(0).hairSprite);
        else
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Hair").transform.GetComponent<Image>().enabled = false;

        if (playerList.GetItem(0).hatSprite != "")
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Hat").transform.GetComponent<Image>().sprite
            = Resources.Load<Sprite>(playerList.GetItem(0).hatSprite);
        else
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Hat").transform.GetComponent<Image>().enabled = false;

        if (playerList.GetItem(0).bodySprite != "")
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Body").transform.GetComponent<Image>().sprite
           = Resources.Load<Sprite>(playerList.GetItem(0).bodySprite);
        else
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Body").transform.GetComponent<Image>().enabled = false;

        if (playerList.GetItem(0).frontArmSprite != "")
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Front-arm").transform.GetComponent<Image>().sprite
           = Resources.Load<Sprite>(playerList.GetItem(0).frontArmSprite);
        else
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Front-arm").transform.GetComponent<Image>().enabled = false;

        if (playerList.GetItem(0).hatSprite != "")
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Hat").transform.GetComponent<Image>().sprite
           = Resources.Load<Sprite>(playerList.GetItem(0).hatSprite);
        else
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Hat").transform.GetComponent<Image>().enabled = false;

        if (playerList.GetItem(0).backHairSprite != "")
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Back-item/Back-hair").transform.GetComponent<Image>().sprite
         = Resources.Load<Sprite>(playerList.GetItem(0).backHairSprite);
        else
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Back-item/Back-hair").transform.GetComponent<Image>().enabled = false;

        if (playerList.GetItem(0).backArmSprite != "")
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Back-item/Back-arm").transform.GetComponent<Image>().sprite
        = Resources.Load<Sprite>(playerList.GetItem(0).backArmSprite);
        else
            GameObject.Find("Profile/PlayerHolder/PlayerOutfit/Back-item/Back-arm").transform.GetComponent<Image>().enabled = false;
    }


    public void HideCanvas()
    {
        mainCanvas.SetActive(false);

    }

    
}
