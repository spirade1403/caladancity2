using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject collisionGameObject = collision.gameObject;
        if (collisionGameObject.tag == "Door")
        {
            NetworkManager n = NetworkManager.instance.GetComponent<NetworkManager>();
            if (n != null) n.CommandDisconnect();
            LoadScene(collisionGameObject.transform.GetChild(0).name);
        }
    }

    public void LoadScene(string scene)
    {
        if (scene != "scene3_WorldMap" && scene.Contains("scene"))
        {
            SceneManager.LoadScene("PersistentScene");
            SceneManager.LoadScene(scene, LoadSceneMode.Additive);
        }
        else
        {
            SceneManager.LoadScene(scene);
        }
    }

    public void OnExistButtonClicked()
    {
        print("leave");
        SceneManager.LoadScene("Login");
        PlayerPrefs.DeleteKey("token");
    }



    //IEnumerator SetActive(string scene)
    //{
    //    Scene sceneLoaded = SceneManager.GetSceneByName(scene);
    //    yield return new WaitForSeconds(1f);
    //}

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {        
        Debug.Log(SceneManager.GetActiveScene().name);
        GameObject door = GameObject.Find(SceneManager.GetActiveScene().name);
        GameObject player = GameObject.FindWithTag("Player");
        if(player != null && door != null)
        {
            player.transform.localPosition = door.transform.position;
        }
    }

}
