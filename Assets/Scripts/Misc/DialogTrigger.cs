using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour
{
    public Message[] messages;
    public Actor[] actors;

    public void StartDialouge()
    {
        FindObjectOfType<DialogManager>().OpenDialogue(messages, actors);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null)
            {
                StartDialouge();
            }
        }
    }

}

[System.Serializable]
public class Message {

    public int actorId;
    public string message;
    public bool haveChoice = false;

}

[System.Serializable]
public class Actor
{
    public string name;
    public Sprite sprite;
}



