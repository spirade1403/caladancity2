using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MoveToMap : MonoBehaviour
{
    [SerializeField] private string sLevelToLoad;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject collisionGameObject = collision.gameObject;
        if (collisionGameObject.name == "Player")
        {
            LoadScene();
        }
    }

    public void LoadScene()
    {
        if (sLevelToLoad != "scene3_WorldMap" && sLevelToLoad.Contains("scene"))
        {
            SceneManager.LoadScene("PersistentScene");
            SceneManager.LoadScene(sLevelToLoad, LoadSceneMode.Additive);
        }
        else
        {
            SceneManager.LoadScene(sLevelToLoad);
        }


    }
}
