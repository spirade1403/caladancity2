using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogManager : MonoBehaviour
{
    public Image actorImage;
    public Text actorName;
    public Text message;
    public RectTransform backgroundBox;
    public RectTransform cancelButton;
    public RectTransform okButton;
    Message[] currentMessages;
    Actor[] currentActors;
    int activeMessage = 0;
    public static bool isActive = false;

    public void OpenDialogue(Message[] messages, Actor[] actors)
    {
        currentMessages = messages;
        currentActors = actors;
        activeMessage = 0;
        isActive = true;
        DisplayMessage();
        backgroundBox.transform.localScale = Vector3.one;
    }

    void DisplayMessage()
    {

        Message messageToDisplay = currentMessages[activeMessage];
        message.text = messageToDisplay.message;
        Actor actorToDislay = currentActors[messageToDisplay.actorId];
        actorName.text = actorToDislay.name;
        actorImage.sprite = actorToDislay.sprite;
        Debug.Log(currentMessages[activeMessage].haveChoice);
        if (currentMessages[activeMessage].haveChoice == true)
        {
            cancelButton.transform.localScale = Vector3.one;
            okButton.transform.localScale = Vector3.one;
        }
        else
        {
            cancelButton.transform.localScale = Vector3.zero;
            okButton.transform.localScale = Vector3.zero;
        }

    }

    public void MoveToHairChanging(string sLevelToLoad)
    {
        Debug.Log("Conversation ended!");
        isActive = false;
        backgroundBox.transform.localScale = Vector3.zero;
        SceneManager.LoadScene(sLevelToLoad);
    }

    public void NextMessage()
    {
        activeMessage++;
        if (activeMessage < currentMessages.Length)
        {
            DisplayMessage();
        }
        else
        {
            Debug.Log("Conversation ended!");
            isActive = false;
            backgroundBox.transform.localScale = Vector3.zero;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        backgroundBox.transform.localScale = Vector3.zero;
        cancelButton.transform.localScale = Vector3.zero;
        okButton.transform.localScale = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isActive == true)
        {
            NextMessage();
        }
    }
}
