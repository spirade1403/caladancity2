using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAssets : MonoBehaviour
{
    private static GameAssets _instances;

    public static GameAssets Instances
    {
        get {
            if (_instances == null) _instances = Instantiate(Resources.Load<GameAssets>("GameAssets"));
            return _instances;
        }
    }

    [SerializeField] public Transform pfChatBubble ;

}
