using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FixedScale : MonoBehaviour
{

    public float FixeScale = 1;
    private Transform parentTransfomr;

    private void Awake()
    {
        parentTransfomr = transform.parent;
    }
    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(FixeScale / parentTransfomr.localScale.x, FixeScale / parentTransfomr.transform.localScale.y, FixeScale / parentTransfomr.transform.localScale.z);

    }
}