using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MoveToWorldMap : MonoBehaviour
{
    [SerializeField] private string sLevelToLoad;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject collisionGameObject = collision.gameObject;
        if (collisionGameObject.name == "Player")
        {
            LoadScene();
        }
    }
    public void LoadScene()
    {

        SceneManager.LoadScene(sLevelToLoad);


    }
}
