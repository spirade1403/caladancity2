using UnityEngine;
using UnityEditor;
using UnityEngine.Tilemaps;

[ExecuteAlways]

public class TileMapGridProperty : MonoBehaviour
{
    private Tilemap tilemap;
    private Grid grid;
    [SerializeField] private SO_GridProperties gridProperty = null;
    [SerializeField] private GridBoolProperty gridBoolProperty = GridBoolProperty.plantable;

    private void OnEnable()
    {
        // Only populate in the editor
        if (!Application.IsPlaying(gameObject))
        {
            tilemap = GetComponent<Tilemap>();

            if(gridProperty != null)
            {
                gridProperty.gridPropertyList.Clear();
            }
        }
    }

    private void OnDisable()
    {   // Only populate in the editor
        if (!Application.IsPlaying(gameObject))
        {
            UpdateGridProperties();    
            tilemap = GetComponent<Tilemap>();

            if (gridProperty != null)
            {
                #if UNITY_EDITOR
                    EditorUtility.SetDirty(gridProperty);
                #endif

            }
        }
    }
    
    private void UpdateGridProperties()
    {
        // Compress timemap buonds
        tilemap.CompressBounds();

        // Only populate in the editor
        if (Application.IsPlaying(gameObject))
        {
            if(gridProperty != null)
            {
                Vector3Int startCell = tilemap.cellBounds.min;
                Vector3Int endCell = tilemap.cellBounds.max;

                for(int x = startCell.x; x < endCell.x; x++)
                {
                    for(int y = startCell.y; y < endCell.y; y++)
                    {
                        TileBase tile = tilemap.GetTile(new Vector3Int(x, y, 0));

                        if(tile != null)
                        {
                            gridProperty.gridPropertyList.Add(new GridProperty(new GridCoordinate(x, y), gridBoolProperty, true));
                        }
                    }
                }
            }
        }
    }

    private void Update()
    {
        if (!Application.IsPlaying(gameObject))
        {
            Debug.Log("DISABLE PROPERTY TILEMAPS");
        }
    }


}
