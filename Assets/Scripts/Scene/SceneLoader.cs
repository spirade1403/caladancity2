using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour 
{
    //public Vector2 playerPosition;
    public SceneName scenceToLoad;

    private void Awake()
    {
        
        Load(scenceToLoad);
        Debug.Log("Loaded");
    }

    public static void Load(SceneName sceneName)
    {
        SceneManager.LoadScene(sceneName.ToString());
    }
}
