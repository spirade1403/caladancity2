using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : MonoBehaviour
{
    [SerializeField] public GameObject settingCanvas;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ShowCanvas()
    {
        settingCanvas.SetActive(true);
    }

    public void HideCanvas()
    {
        settingCanvas.SetActive(false);
    }

    public void logOut()
    {

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
