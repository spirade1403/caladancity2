using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputSystem : MonoBehaviour
{
    private Rigidbody2D rigidbody2D;
    private PlayerInput playerInput;

    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        playerInput = GetComponent<PlayerInput>();

        PlayerInputActions playerInputActions = new PlayerInputActions();
        playerInputActions.Player.Enable();
        playerInputActions.Player.Movement.performed += Movement_performed;
    }

    private void Movement_performed(InputAction.CallbackContext context)
    {
        Debug.Log(context);
        Vector2 inputVector = context.ReadValue<Vector2>();
        float speed = 20f;
        //rigidbody2D.AddForce(new Vector3(inputVector.x, inputVector.y, 0) * speed, ForceMode2D.Impulse);
        rigidbody2D.MovePosition(rigidbody2D.position + inputVector * speed * Time.fixedDeltaTime);
    }

    private void Sit_performed(InputAction.CallbackContext context)
    {
        Debug.Log(context);
        Animator animator = GetComponent<Animator>();
        if (animator.GetBool("isSitting"))
        {
            animator.SetBool("isSitting", false);
        }
        else
        {
            animator.SetBool("isSitting", true);
        }
    }
}
