using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeSprite : MonoBehaviour
{
    // Start is called before the first frame update
   [SerializeField] public SpriteRenderer bodyPart;
   [SerializeField] public Sprite sprite;
    void Start()
    {
        bodyPart.sprite = sprite;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
